$(document).ready(function(){
	//아이템 하단 버튼 클릭시 액티브 클래스를 추가해주세요.
//	$(".item-footer a").click(function(){
//		if(!$(this).hasClass("pop")){
//			$(this).toggleClass("active");
//			return false;
//		}else{
//			$(this).children(".count").animate({
//				backgroundSize:22
//			},100,function(){
//				$(this).animate({backgroundSize:18});
//			});
//			$(this).next(".pop-menu").toggleClass("active");
//			return false;
//		}
//	});
	
	//popcon아이콘 애니메이션
	//클릭이벤트 안에 넣어주세요
	$(".button.pop").click(function(){
	if($(this).hasClass('pop')){
	$(this).children(".count").animate({
	backgroundSize:22
	},100,function(){
	$(this).animate({backgroundSize:18});
	});
	}
	});
	
	//팝메뉴클릭시 온오버.
//	$("a.name").click(function(){
//		$(this).next(".pop-menu").toggleClass("active");
//		return false;
//	});
	
	//네비게이션 셀렉트박스
	$("#nav-category").change(function(){
        var selectText = $(this).children("option:selected").text();
        $(this).siblings("label").text(selectText);
    });
	
	//카테고리 셀렉트박스
	$(".category>select").change(function(){
        var selectText = $(this).children("option:selected").text();
        $(this).siblings("label").text(selectText);
    });
	
	//메인비주얼 플러그인 실행
	$(".main-visual-list").bxSlider();
	
	//인풋클리어
	var inputText = $(".input>label").next("input[type='text']");
	inputText
		.focus(function(){
			$(this).prev("label").css("visibility","hidden");
		})
		.blur(function(){
			if($(this).val() == ""){
				$(this).prev("label").css("visibility","visible");
			} else {
				$(this).prev("label").css("visibility","hidden");
			}
		})
		.change(function(){
			if($(this).val() == ""){
				$(this).prev("label").css("visibility","visible");
			} else {
				$(this).prev("label").css("visibility","hidden");
			}
		})
		.blur();
});

//네비게이션 메뉴 컨트롤
function gnbMenu(item){
	$(item).parent().toggleClass("opened");
}

//$(window).scroll(function(){
//	if($(window).scrollTop() >= $(document).height()-$(window).height()){
//		//스크롤 최하단 도달했을 때
//		//item이라는 클래스를 가진 요소를 복사해서
//		var $items = $(".item").clone();
//		//grid라는 id를 가진 요소안에 append시키고 append된 요소를 다시 재배치
//		$("#grid").append($items).masonry('appended',$items);
//	}
//});