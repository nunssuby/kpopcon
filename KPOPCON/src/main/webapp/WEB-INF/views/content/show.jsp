<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<div class="notice-area">
	<span>Kpopcon noticed in here!!!! tangerine has solved some of the most complex puzzles in design through..</span>
	<a href="#">Click me!</a>
</div>
<jsp:directive.include file="/WEB-INF/views/header.jsp"/>
<section id="contents">
	<article id="detail">
		<div class="detail-photo">
			<c:choose>
		       <c:when test="${content.contentType == 'Video'}">
		           <video  width="320" height="240"  controls>
					  <source src="<c:url value="/image/${content.contentFiles[0].fileId}"/>" type="video/mp4">
					</video>
		       </c:when>
		       <c:when test="${content.contentType == 'Image'}">
		       		<img src="<c:url value="/image/${content.contentFiles[0].fileId}"/>" />
		       </c:when>
		       <c:when test="${content.contentType == 'Audio'}">
			       	<audio controls>
					  <source src="<c:url value="/image/${content.contentFiles[0].fileId}"/>" type="audio/mp3">
					</audio>
		       </c:when>
		       <c:when test="${content.contentType == 'Text'}">
		       		<p>${content.description}</p>
		       </c:when>
		       <c:when test="${content.contentType == 'Link'}">
					  <object title="YouTube video player" class="youtube-player" type="text/html"  data="https://www.youtube.com/embed/${content.youtubeLink}" allowFullScreen></object>
		       </c:when>
		       <c:otherwise>
		       </c:otherwise>
		   </c:choose>
		</div>
		<div class="detail-btns">
			<ul>
				<li>
					<a href="#" class="like" title="Like">
						<em>I Like it</em>
						<span class="count">20</span>
					</a>
				</li>
				<li>
					<a href="#" class="share" title="Share">
						<em>SNS</em>
						<span class="count">100</span>
					</a>
				</li>
				<li>
					<a href="#" class="comments" title="Comments">
						<em>Comments</em>
						<span class="count">1000</span>
					</a>
				</li>
				<li>
					<a href="#" class="popcon" title="Popcon">
						<em>Popcon</em>
						<span class="count">312</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="detail-content">
			<h1>${content.title}</h1>
			<p>
				${content.description}
			</p>
			<div class="detail-content-info">
				<div class="author">
					<span class="profile"><img src="${pageContext.request.contextPath}/resources/img/temporaray_profile.png" alt=""></span>
					<a class="name" href="#">${content.member.name}</a>
					<div class="pop-menu user">
						<ul>
							<li><a href="#">친구맺기</a></li>
							<li><a href="#">차단</a></li>
							<li><a href="#">BOX방문</a></li>
							<li><a href="#">쪽지</a></li>
						</ul>
					</div>
					<span class="date">${content.creationDate}</span>			
					<span class="state"><img src="${pageContext.request.contextPath}/resources/img/icon_state_us.png" alt="USA"></span>
				</div>
				<div class="tags">
					<a href="#">AOA</a>,<a href="#">KPOPCON</a>
				</div>
				<div class="post">
					<div class="btns">
						<a href="#" class="btn-report" title="Report"><span class="blind">report</span></a><a href="#" class="btn-modify" class="modify" title="Modify"></a><a href="#" class="btn-delete" class="delete" title="Delete"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="detail-comments">
			<form:form id="commentForm" name="commentForm" method="POST" modelAttribute="comment" enctype="multipart/form-data">
				<input type="hidden" name="contentId" value="${content.contentId}">
				<div class="detail-comments-write">
					<label for="comment-input">Comment</label>
					<div class="input-comment"><input type="text" id="commnet-input" name="comment"></div>
					<div class="btn-wrap">
						<input type="checkbox" id="comment-secret" name="isSecret" value="true">
						<label for="comment-secret">Secret</label>
						<a href="javascript:$('#commentForm').submit()" class="btn-save"><span class="blind">Save</span></a>
					</div>
				</div>
			</form:form>
			<ul class="detail-comments-list">
				<c:forEach var="comment" items="${content.comments}" varStatus="status">
					<li>
						<div class="comment-writer">
							<span class="user-photo"><img src="${pageContext.request.contextPath}/resources/img/default_profile-small.png" alt=""></span>
							<a class="name" href="#">${comment.memberName}</a>
							<div class="pop-menu comment-user">
								<ul>
									<li><a href="#">Follow</a></li>
									<li><a href="#">Unfollow</a></li>
									<li><a href="#">Invite</a></li>
									<li><a href="#">Visit On Centents</a></li>
								</ul>
							</div>
						</div>
						<div class="comment">
							<span>${comment.comment}</span>
						</div>
						<div class="info">
							<span class="date">${comment.creationDate}</span>
							<span class="time">${comment.creationTime}</span>
							<span class="btns">
								<a href="#" class="modify" title="Modify"></a>
								<a href="#" class="delete" title="Delete"></a>
							</span>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>
	</article>
</section>
<footer id="footer"></footer>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>
<script type="text/javascript">
	//이미지를 모두 불러왔을 때, 아이템을 재배치
	$("img").one("load", function() {
		$('#grid').masonry({
			itemSelector: '.item',
			columnWidth: '.item',
			percentPosition: true
		});
		//네번째 아이템을 선택해서 데스크탑일 때 margin-top을 줍니다.
		$('.item:nth-child(4)').addClass("add_margin");
		}).each(function() {
			if(this.complete) $(this).load();
		});
	
/* 	function submitCommentForm(){
		var data = $("#messageForm").serialize();
		$.ajax({      
	        type:"post",  
	        url:"${pageContext.request.contextPath}/message",      
	        data:data,      
	        success:function(args){   
	        	if(args.result)
					$(location).attr('href','<c:url value="/"/>');
				else 
					alert("메세지 발송 실패");
	        },   
	        error:function(e){  
	            alert(e.responseText);  
	        }  
	    }); 
	} */
	
/* 	function addComment(){
		alert("a");
		var data = $("#commentForm").serialize();
		$.ajax({      
	        type:"POST",  
	        url:"${pageContext.request.contextPath}/content/comment",      
	        data:data,      
	        success:function(args){   
	        	console.log(args);
	        },   
	        error:function(e){  
	            alert(e.responseText);  
	        }  
	    });  
	} */
 	    $('#commentForm').submit( function( e ) {
	    	  $.ajax({
	      	    url: "${pageContext.request.contextPath}/content/comment",
	      	    type: 'POST',
	      	    	data: new FormData( this ),
	      	    contentType: false,
	      	    processData: false,
//	       	    dataType: 'json',
	      	    success: callback,
	      	    error : contentError,
	      	  	beforeSend : function(xmlHttpRequest){
                  xmlHttpRequest.setRequestHeader("AJAX", "true"); // ajax 호출을  header에 기록
				}
	      	});
	        e.preventDefault();
	    } );  
	    
	    function callback(comment, status){
	    	if(status=='success'){
	    		alert("댓글이 등록되었습니다.");
	    		location.reload();
	    			
	    	}
	    }
	    function contentError(xhr, textStatus, error){
	    	if(xhr.status=="500"){
        		$(location).attr('href','${pageContext.request.contextPath}/auth/login/');
        	}else{
        		alert("code:"+xhr.status+"\n"+"message:"+xhr.responseText+"\n"+"error:"+error);
        	}
	    } 
	     
	    
	
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>