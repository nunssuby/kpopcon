<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sub.css">
</head>
<body>
<div class="layer-wrap">
		<section id="upload" class="layer">
		<header class="layer-header">
			<h1>Register Contents</h1>
		</header>
		<form:form id="contentForm" name="contentForm" method="POST" enctype="multipart/form-data" modelAttribute="content">
			<input type="hidden" name="contentType" value="${contentType}">
			<div class="layer-content">
				<dl class="title">
					<dt><label for="upload-title">Title</label></dt>
					<dd>
						<div class="input-wrap">
							<input id="upload-title" name="title" type="text" required>
						</div>
					</dd>
				</dl>
				<dl class="file">
					<dt><label for="upload-file">File</label></dt>
					<dd>
						<div class="file-btn">
							<input id="upload-file" type="file" name="files" multiple>
						</div>
					</dd>
				</dl>
				<dl class="youtube">
					<dt><label for="upload-link">Youtube Link</label></dt>
					<dd>
						<div class="input-wrap">
							<input id="upload-link" type="text" name="youtubeLink">
						</div>
					</dd>
				</dl>
				<dl class="category">
					<dt>Category</dt>
					<dd>
						<div class="upload-select">
							<label for="upload-category">Category</label>
							<select id="upload-category" title="select category" name="categoryId" required>
								<option value="" >Category</option>
								<c:forEach var="category" items="${categories}" varStatus="status">
									<option value="${category.value}">${category}</option>
								</c:forEach>
							</select>
						</div>
					</dd>
				</dl>
				<dl class="desc">
					<dt><label for="upload-desc">Description</label></dt>
					<dd>
						<div class="textarea-wrap">
							<textarea id="upload-desc" name="description"></textarea>
						</div>
					</dd>
				</dl>
				<dl class="box">
					<dt>Contents Box</dt>
					<dd>
						<div class="upload-select">
							<label for="upload-box">Box</label>
							<select id="upload-box" name="contentBoxId">
								<option value="" selected="selected">Box</option>
								<c:forEach var="contentBox" items="${contentBoxes}" varStatus="status">
									<option value="${contentBox.boxId}">${contentBox.name}</option>
								</c:forEach>
							</select>
						</div>
					</dd>
				</dl>
				<dl class="tag">
					<dt><label for="upload-tag">Tag</label></dt>
					<dd>
						<div class="input-wrap">
							<input id="upload-tag" type="text" name="tagWithComma">
						</div>
					</dd>
				</dl>
				<dl class="setting">
					<dt>Public Setting</dt>
					<dd>
						<ul class="radio-wrap">
							<li>
								<input id="setting-radio1" name="scopeId" type="radio" value="1">
								<label for="setting-radio1">모두 공개</label>
							</li>
							<li>
								<input id="setting-radio2" name="scopeId" type="radio" value="2">
								<label for="setting-radio2">친구 공개</label>
							</li>
							<li>
								<input id="setting-radio3" name="scopeId" type="radio" value="3">
								<label for="setting-radio3">비공개</label>
							</li>
						</ul>
						<span class="sns-check">
							<input id="sns" type="checkbox" name="isSns">
							<label for="sns">SNS</label>
						</span>
					</dd>
				</dl>
			</div>
			<footer class="layer-footer">
				<a class="layer-btn submit" href="javascript:$('#contentForm').submit()" >Register</a>
				<a class="layer-btn" href="javascript:history.go(-1)();">Cancel</a>
			</footer>
		</form:form>
	</section>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>
<script>
/*       function create(form){
// 		var data = $("#contentForm").serialize();
		var form = $("#contentForm");
		var data = new FormData(form);
		console.log(data);
		$.post("<c:url value='/content'/>", data ,function(content, status) {
	//			alert("Data: " + data + "\nStatus: " + status);
			if(status=='success'){
				if($.isNumeric(content.contentId)&&content.contentId>0)
					alert("content를 더 올리겠습니까");
// 				else ;
					// TODO 로그인 false 구현
			} else ;
				// TODO 로그인 실패 구현
		});
    }   */ 
    
    
    $( document ).ready(function() {
	  	$('#upload-pop').hide();
	});
    
    $(document).ready(function(){
    	var contentType='${contentType}';
    	if(contentType=='Link')
    		$('.file').hide();
    	else
    		$('.youtube').hide();
    });
    
    
    $('#contentForm').submit( function( e ) {
    	
    	  $.ajax({
      	    url: "<c:url value='/content'/>",
      	    type: 'POST',
      	    	data: new FormData( this ),
      	    contentType: false,
      	    processData: false,
//       	    dataType: 'json',
      	    success: callback,
      	    error : contentError
      	});
        e.preventDefault();
    } );  
    
    function callback(contentId, status){
    	if(status=='success'){
    		if($.isNumeric(contentId)&&contentId>0){
    			if($("#isSns").is(":checked")){
					if(loginType = 'TW')
						postTwitter(contentId);
				}else{
					if(confirm("content를 더 올리겠습니까") == true){
						$('#contentForm')[0].reset();
					}else
						$(location).attr('href','<c:url value="/"/>');
				}
    			
    		}
    			
    	}
    }
    function contentError(request,status,error){
    	alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
    }
     
    function postTwitter(contentId){
		var url = 'http://211.253.11.224/kpopcon/content/'+contentId;
		var content = 'KPOPCON : '+url;
		OAuth.popup('twitter')
		.done(function(result) {
		    result.post('/1.1/statuses/update.json', {
		        data: {
		        	status: content
		        }
		    })
		    .done(function (response) {
		        //this will display the id of the message in the console
		        console.log(response.id);
		        if(confirm("content를 더 올리겠습니까") == true){
					$('#contentForm')[0].reset();
				}else
					$(location).attr('href','<c:url value="/"/>');
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		    //handle error with err
		});
	}
      
      
     
/*      $('#contentForm')
     .submit( function( e ) {
    	 create(this);
       e.preventDefault();
     } );  */
    
/*      $('#contentForm')
    .submit( function( e ) {
      $.ajax( {
        url: "<c:url value='/content'/>",
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false
      } );
      e.preventDefault();
    } );  */
    
    
    </script>

<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>

