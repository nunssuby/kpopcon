<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sub.css">
</head>
<body>
<div class="notice-area">
	<span>Kpopcon noticed in here!!!! tangerine has solved some of the most complex puzzles in design through..</span>
	<a href="#">Click me!</a>
</div>
<jsp:directive.include file="/WEB-INF/views/header.jsp"/>
<section id="contents" class="wide message">
	<header class="message-header">
		<h1>View Message</h1>
	</header>
	<div class="message-comments">
		<ul class="message-comments-list">
			<c:forEach var="message" items="${messages}" varStatus="status">
			<li>
				<div class="comment-writer">
					<span class="user-photo"><img src="${pageContext.request.contextPath}/resources/img/default_profile-small.png" alt=""></span>
					<a class="name" href="#">${message.sender.name}</a>
				</div>
				<div class="comment">
					<span>${message.message}</span>
				</div>
				<div class="info">
					<span class="date">${message.creationDate}</span>
					<span class="time">${message.creationTime}</span>
					<span class="btns">
						<a href="#" class="delete" title="Delete"></a>
					</span>
				</div>
			</li>
			</c:forEach>
		</ul>
	</div>
</section>
<footer id="footer"></footer>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>
<script type="text/javascript">
	$(window).load(function(){
		$('#grid').masonry({
			itemSelector: '.item',
			columnWidth: '.item',
			percentPosition: true
		});
	});
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>