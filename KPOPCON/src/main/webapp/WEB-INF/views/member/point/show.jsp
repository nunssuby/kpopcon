<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sub.css">
</head>
<body>
<div class="layer-wrap">
		<section id="my-point" class="layer">
		<header class="layer-header">
			<h1>My Point</h1>
		</header>
		<div class="layer-content">
			<p class="description">Point 산정은 Poping Contents에 대한 좋아요와 Pop 수를 합산한 것입니다.<br>
			이 포인트는 경매 참가 시에 사용할 수 있습니다.</p>
			<dl class="point">
				<dt>Your Point is :</dt>
				<dd><span class="count">100</span></dd>
			</dl>
			<div class="point-detail">(Like it <span>50</span> + Pop <span>50</span>)</div>
		</div>
		<footer class="layer-footer">
			<a class="layer-btn" href="${pageContext.request.contextPath}/member/setting">OK</a>
		</footer>
	</section>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>

<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>