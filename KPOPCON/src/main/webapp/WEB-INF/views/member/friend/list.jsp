<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sub.css">
</head>
<body>
<div class="layer-wrap">
		<section id="friend-list" class="layer">
		<header class="layer-header">
			<h1>Like/Disklike User</h1>
		</header>
		<div class="layer-content">
			<ul>
				<c:forEach var="friend" items="${friends}" varStatus="status">
					<li>
						<div class="user">
							<div class="profile"></div>
							<a href="#" class="name">${friend.friendName}</a>
						</div>
						<span class="info">${friend.status}</span>
					</li>
				</c:forEach>
			</ul>
		</div>
	</section>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>


<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>