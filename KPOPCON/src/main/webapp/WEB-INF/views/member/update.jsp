<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sub.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
</head>
<body>
<div class="layer-wrap">
		<section id="add-info" class="layer">
		<header class="layer-header">
			<h1>Add Your Information</h1>
		</header>
		<c:url var="post_url"  value="/member/information" />
		<form:form id="informationForm" action="${post_url}" name="informationForm" modelAttribute="member" method="put">
		<input type="hidden" name="memberId" value="${member.memberId}">
			<div class="layer-content">
				<dl class="name">
					<dt><label for="add-info-name">Name</label></dt>
					<dd>
						<div class="input-wrap">
							<input id="add-info-name" type="text" name="name" required>
						</div>
					</dd>
				</dl>
				<dl class="gender">
					<dt>Gender</dt>
					<dd>
						<div class="radio-wrap">
							<input id="add-info-gender1" type="radio" name="gender" value="male" required>
							<label for="add-info-gender1">male</label>
							<input id="add-info-gender2" type="radio" name="gender" value="female">
							<label for="add-info-gender2">female</label>
						</div>
					</dd>
				</dl>
				<dl class="birth">
					<dt><label for="add-info-birth">Birthday</label></dt>
					<dd>
						<div class="input-wrap">
							<input id="add-info-birth" type="text" placeholder="month / day / year" name="birthday" required>
						</div>
					</dd>
				</dl>
				<dl class="nation">
					<dt>Nationality</dt>
					<dd>
						<div class="category">
							<label for="add-info-nation"></label>
							<select id="add-info-nation" name="nationalityId" required>
								<option value="1">중국 </option>
								<option value="2">한국 </option>
								<option value="3">미국 </option>
							</select>
						</div>
					</dd>
				</dl>
			</div>
			<footer class="layer-footer">
				<a class="layer-btn complete" href="javascript:submitForm()">Complete</a>
			</footer>
		</form:form>
	</section>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>
<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
<!-- Datepicker 관련 소스 -->

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>

//########### jquery UI DatePicker 캘린더 호출 및 설정 시작. ##############
/* $(".datepicker").datepicker({
showOn: "both", // 버튼과 텍스트 필드 모두 캘린더를 보여준다.
buttonImage: "/application/db/jquery/images/calendar.gif", // 버튼 이미지
buttonImageOnly: true, // 버튼에 있는 이미지만 표시한다.
changeMonth: true, // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
changeYear: true, // 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
minDate: '-100y', // 현재날짜로부터 100년이전까지 년을 표시한다.
nextText: '다음 달', // next 아이콘의 툴팁.
prevText: '이전 달', // prev 아이콘의 툴팁.
numberOfMonths: [1,1], // 한번에 얼마나 많은 월을 표시할것인가. [2,3] 일 경우, 2(행) x 3(열) = 6개의 월을 표시한다.
//stepMonths: 3, // next, prev 버튼을 클릭했을때 얼마나 많은 월을 이동하여 표시하는가. 
yearRange: 'c-100:c+10', // 년도 선택 셀렉트박스를 현재 년도에서 이전, 이후로 얼마의 범위를 표시할것인가.
showButtonPanel: true, // 캘린더 하단에 버튼 패널을 표시한다. 
currentText: '오늘 날짜' , // 오늘 날짜로 이동하는 버튼 패널
closeText: '닫기',  // 닫기 버튼 패널
dateFormat: "yy-mm-dd", // 텍스트 필드에 입력되는 날짜 형식.
showAnim: "slide", //애니메이션을 적용한다.
showMonthAfterYear: true , // 월, 년순의 셀렉트 박스를 년,월 순으로 바꿔준다. 
dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'], // 요일의 한글 형식.
monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] // 월의 한글 형식.
                  
}); */
//########### jquery UI DatePicker 캘린더 호출 및 설정 시작. ##############

	$(function() {
		$( "#add-info-birth" ).datepicker({
			showOn: "button",
			buttonImage: "${pageContext.request.contextPath}/resources/img/icon_calendar.png",
			buttonImageOnly: true,
			buttonText: "Select date",
			changeMonth: true, // 월을 바꿀수 있는 셀렉트 박스를 표시한다.
			changeYear: true, // 년을 바꿀 수 있는 셀렉트 박스를 표시한다.
// 			minDate: '-60y', // 현재날짜로부터 100년이전까지 년을 표시한다.
			yearRange: 'c-40:c+0', // 년도 선택 셀렉트박스를 현재 년도에서 이전, 이후로 얼마의 범위를 표시할것인가.
			dateFormat: "yy-mm-dd"
		});
	});
/* 	$(function() {
		$("informationForm").validate();
	}) */
	
/* 	function validation(){
	if($('#add-info-name').val()==''){
		alert("이름을 입력하세");
		return;
	}
		
	if($('#add-info-birth').val()=='')
		return;
	if($('#add-info-name').val()=='')
		return;
	} */
	
	function submitForm(){
// 		validation();
		$('#informationForm').submit();
	}
	
// 	$(function() {
// 	    $( "#datepicker" ).datepicker({
// 	      showOn: "button",
// 	      buttonImage: '<c:url value="/resources/images/calendar.png"/>',
// 	      buttonImageOnly: true,
// 	      buttonText: "Select date",
// 	      dateFormat: "yy-mm-dd"
// 	    });
// 	});
</script>
<!--// Datepicker 관련 소스 -->

<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>





