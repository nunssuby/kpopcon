<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<div class="notice-area">
	<span>Kpopcon noticed in here!!!! tangerine has solved some of the most complex puzzles in design through..</span>
	<a href="#">Click me!</a>
</div>
<jsp:directive.include file="/WEB-INF/views/header.jsp"/>
<section id="contents" class="setting">
	<div class="wide-visual">
		<article class="main-visual">
			<ul class="main-visual-list">
				<li><img src="${pageContext.request.contextPath}/resources/img/temporary_banner.png" alt=""></li>
				<li><img src="${pageContext.request.contextPath}/resources/img/temporary_banner.png" alt=""></li>
				<li><img src="${pageContext.request.contextPath}/resources/img/temporary_banner.png" alt=""></li>
			</ul>
		</article>
	</div>
	<article class="setting">
		<div class="setting-wrap">
			<header class="setting-header">
				<h1>My Page</h1>
				<span class="point">
					<span class="count">1,200</span>
					<a class="btn_info" href="${pageContext.request.contextPath}/member/point"><span class="blind">information</span></a>
				</span>
			</header>
			<c:url var="post_url"  value="/member/setting" />
			<form:form id="settingForm" action="${post_url}" name="settingForm" modelAttribute="member" method="post" enctype="multipart/form-data">
			<input type="hidden" name="memberId" value="${member.memberId}">
				<div class="setting-content">
					<div class="profile">
						<div class="photo blank">
							<c:if test="${member.profileFileName != null }">
								<img src="${pageContext.request.contextPath}/image/profile/${member.memberId}">
							</c:if>
						</div>
						<div class="btn">
							<input type="file" name="file">
						</div>
					</div>
					<div class="info">
						<dl class="id">
							<dt>Email(ID)</dt>
							<dd>
								<span class="id">${member.email}</span>
								<a class="btn pw-change" href="${pageContext.request.contextPath}/member/password"><span class="blind">Change Password</span></a>
							</dd>
						</dl>
						<dl class="name">
							<dt><label for="setting-name">Name</label></dt>
							<dd>
								<div class="input-wrap">
									<input id="setting-name" type="text" value="${member.name}" name="name" required>
								</div>
							</dd>
						</dl>
						<div class="dl-column">
							<dl class="birth">
								<dt>Birthday</dt>
								<dd>${member.birthday}</dd>
							</dl>
							<dl class="birth">
								<dt>Gender</dt>
								<dd>${member.gender}</dd>
							</dl>
						</div>
						<dl class="state">
							<dt>Nationality</dt>
							<dd>
								<span class="state-icon ko">${member.nationality.nationality}<img src="${pageContext.request.contextPath}/resources/img/nationality/${member.nationality.nationality}.png" alt="USA"></span>
							</dd>
						</dl>
						<dl class="intro">
							<dt><label for="setting-intro">Introduction</label></dt>
							<dd>
								<div class="input-wrap">
									<input id="setting-intro" type="text" value="${member.introduction}" name="introduction" >
								</div>
							</dd>
						</dl>
						<dl class="mail">
							<dt>Mail</dt>
							<dd>
								<span class="radio radio1">
									<input id="mail1" name="checkMail" type="radio" value="true">
									<label for="mail1">Yes, I want to receive a mails</label>
								</span>
								<span class="radio radio2">
									<input id="mail2" name="checkMail" type="radio" value="false">
									<label for="mail2">No</label>
								</span>
							</dd>
						</dl>
					</div>
				</div>
				<footer class="setting-footer">
					<a class="setting-btn save" href="javascript:submitSettingForm();">Save</a>
					<a class="setting-btn" href="${pageContext.request.contextPath}/member/friend?type=friend">View Like Users</a>
					<a class="setting-btn" href="${pageContext.request.contextPath}/member/friend?type=dislike">View Dislike Users</a>
				</footer>
			</form:form>
		</div>
	</article>
</section>
<footer id="footer"></footer>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>
<script type="text/javascript">
	$(window).load(function(){
		$('#grid').masonry({
			itemSelector: '.item',
			columnWidth: '.item',
			percentPosition: true
		});
	})
	
	$(document).ready(function(){
		var checkMail = '${member.checkMail}';
		if(checkMail == 'true'){
			$("input:radio[id='mail1']").attr("checked", true); /* by ID */
		}else{
			$("input:radio[id='mail2']").attr("checked", true); 
		}
		
		
	});
	
	function submitSettingForm(){
		$('#settingForm').submit();
/* 		var formData = $('#settingForm').serialize();
		$.ajax({
			type : 'post',
			url : "${pageContext.request.contextPath}/member/setting",
			data : formData,
			success : function (member,status){
				if(status='success')
					alert(member.id);
			},
			error : function (e){
				alert(e);
			}
			
		}); */
	}
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>