<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="true"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="<c:url value="/resources/js/html5shiv.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/reset.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/common.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/main.css"/>">
</head>
<body>
	<div class="notice-area">
		<span>Kpopcon noticed in here!!!! tangerine has solved some of the most complex puzzles in design through..</span> <a href="#">Click me!</a>
	</div>
	<jsp:directive.include file="/WEB-INF/views/header.jsp"/>
	<section id="contents">
		<div class="wrap wrap-upper">
			<article class="main-visual">
				<ul class="main-visual-list">
					<li><img src="${pageContext.request.contextPath}/resources/img/temporary_banner.png" alt=""></li>
					<li><img src="${pageContext.request.contextPath}/resources/img/temporary_banner.png" alt=""></li>
					<li><img src="${pageContext.request.contextPath}/resources/img/temporary_banner.png" alt=""></li>
				</ul>
			</article>
			<article class="feed">
				<ul>
					<li class="like"><a href="${pageContext.request.contextPath}/?orderBy=like_count">I Like it<span>${totalLikeCount}</span></a></li>
					<li class="sns"><a href="${pageContext.request.contextPath}/?orderBy=share_count">SNS<span>${totalShareCount}</span></a></li>
					<li class="pop"><a href="${pageContext.request.contextPath}/">Pop!<span>0</span></a></li>
					<li class="comments"><a href="${pageContext.request.contextPath}/?orderBy=comment_count">Comments<span>${totalCommentCount}</span></a></li>
				</ul>
			</article>
		</div>
		<div class="wrap wrap-lower">
			<article class="popular-search">
				<div class="popular-search-wrap">
					<h1>Popular Searches</h1>
					<ol>
						<li><a href="#">KpopCON Style</a></li>
						<li><a href="#">gangnam style Music</a></li>
						<li><a href="#">AOA MUSIC START</a></li>
						<li><a href="#">KpopCON Style</a></li>
						<li><a href="#">KpopCON Style KpopCON Style</a></li>
					</ol>
				</div>
			</article>
			<section id="grid">
				<c:forEach var="content" items="${contents}" varStatus="status">
					<article class="item text">
						<div class="item-wrap">
							<header class="item-header">
								<div class="author">
									<div class="profile">
										<img src="${pageContext.request.contextPath}/resources/img/temporaray_profile.png" alt="">
									</div>
									<c:choose>
										<c:when test="${member.memberId == content.contentMemberId}">
											<a class="name" href="javascript:void(0)">${content.contentMemberName}</a>
										</c:when>
										<c:otherwise>
											<a class="name" href="javascript:userPop(${content.contentId},${content.contentMemberId});">${content.contentMemberName}</a>
										</c:otherwise>
									</c:choose>
									<div class="pop-menu user" id="notYetFriend_${content.contentId}">
										<ul>
											<li><a href="javascript:addFriend(${content.contentMemberId});">친구맺기</a></li>
											<li><a href="#">차단</a></li> 
											<li><a href="#">BOX방문</a></li>
										</ul>
									</div>
									<div class="pop-menu user" id="friend_${content.contentId}">
										<ul>
											<li><a href="#">친구해제</a></li>
											<li><a href="#">BOX방문</a></li>
											<li><a href="javascript:sendMessage(${content.contentMemberId});">쪽지</a></li>
										</ul>
									</div>
									<div class="pop-menu user" id="dislike_${content.contentId}">
										<ul>
											<li><a href="#">차단해제</a></li>
											<li><a href="#">BOX방문</a></li>
										</ul>
									</div>
									<span class="state"><img src="${pageContext.request.contextPath}/resources/img/icon_state_us.png" alt="USA"></span>
								</div>
								<div class="description">
									<span>${content.title}</span> <span class="date">${content.creationDate}</span>
								</div>
							</header>
							<a href="${pageContext.request.contextPath}/content/${content.contentId}">
							<div class="item-view">
								<c:choose>
							       <c:when test="${content.contentType == 'Video'}">
							           <video controls>
										  <source src="<c:url value="/image/${content.mainFile.fileId}"/>" type="video/mp4">
										</video>
							       </c:when>
							       <c:when test="${content.contentType == 'Image'}">
							       		<img src="<c:url value="/image/${content.mainFile.fileId}"/>" />
							       </c:when>
							       <c:when test="${content.contentType == 'Audio'}">
								       	<audio controls>
										  <source src="<c:url value="/image/${content.mainFile.fileId}"/>" type="audio/mp3">
										</audio>
							       </c:when>
							       <c:when test="${content.contentType == 'Text'}">
							       		<p>${content.description}</p>
							       </c:when>
							       <c:when test="${content.contentType == 'Link'}">
										  <object title="YouTube video player" class="youtube-player" type="text/html"  data="https://www.youtube.com/embed/${content.youtubeLink}" allowFullScreen></object>
<%-- 							      		<iframe src="//www.youtube.com/embed/${content.youtubeLink}" frameborder="0" allowfullscreen></iframe> --%>
							       </c:when>
							       <c:otherwise>
							       </c:otherwise>
							   </c:choose>
							</div>
							</a>
							<footer class="item-footer">
								<c:choose>
									<c:when test="${content.isLike == 'true'}">
										<a id="like_${content.contentId}" class="button like active" href="javascript:toggleLike('${content.isLike}',${content.contentId})" title="like"> <span id="like_span_${content.contentId}" class="count">${content.likeCount}</span></a> 
									</c:when>
									<c:otherwise>
										<a id="like_${content.contentId}" class="button like" href="javascript:toggleLike('${content.isLike}',${content.contentId})" title="like"> <span id="like_span_${content.contentId}" class="count">${content.likeCount}</span></a> 
									</c:otherwise>
								</c:choose>
								
								<a class="button share" href="#" title="share"> <span class="count">${content.shareCount}</span>
								</a> <a class="button pop" href="javascript:clickpop()" title="pop"> <span class="count">25</span>
								</a>
								<div class="pop-menu popcon">
									<ul>
										<li><a href="#">Default</a></li>
										<li><a href="#">Food</a></li>
										<li><a href="#">Culture</a></li>
										<li><a href="#">Movie</a></li>
									</ul>
								</div>
								<c:choose>
									<c:when test="${content.isComment == 'true'}">
										<a class="button comments active" href="#" title="comments"> <span class="count">${content.commentCount}</span></a>
									</c:when>
									<c:otherwise>
										<a class="button comments" href="#" title="comments"> <span class="count">${content.commentCount}</span></a>
									</c:otherwise>
								</c:choose>
								
								
							</footer>
						</div>
					</article>
				</c:forEach>
			</section>
		</div>
		<form:form id="likeform" name="likeform" action="${pageContext.request.contextPath}/content/like" enctype="multipart/form-data" modelAttribute="contentRelationship">
			<input type="hidden" id="contentId" name="contentId">
			<input type="hidden" id="isAdd" name="isAdd">
		</form:form>
	</section>

	<footer id="footer"></footer>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/masonry.pkgd.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.bxslider.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/default.js"/>"></script>

	<script>
		//이미지를 모두 불러왔을 때, 아이템을 재배치
		$("img").one("load", function() {
			$('#grid').masonry({
				itemSelector : '.item',
				columnWidth : '.item',
				percentPosition : true
			});
			//네번째 아이템을 선택해서 데스크탑일 때 margin-top을 줍니다.
			$('.item:nth-child(4)').addClass("add_margin");
		}).each(function() {
			if (this.complete)
				$(this).load();
		});
		//업로드 버튼 애니메이션
/*  	$(function(){
		$(".upload-icon>a").each(function(index){
			
			$(this).delay(150*index).animate({
				top:'-=7',
				left:'-=7',
				width:'+=14',
				height:'+=14'
			},300);
			$(this).delay().animate({
				top:'+=7',
				left:'+=7',
				width:'-=14',
				height:'-=14'
			},300);
		})
	}) */
		
		
		
		//여기부터 개발자 영역
		var startRow = 0;
		var endRow = 9;
/* 		$(window).scroll(function(){
			if($(window).scrollTop() >= $(document).height()-$(window).height()){
				//스크롤 최하단 도달했을 때
				//item이라는 클래스를 가진 요소를 복사해서
		//		var $items = $(".item").clone();
		//		//grid라는 id를 가진 요소안에 append시키고 append된 요소를 다시 재배치
		//		$("#grid").append($items).masonry('appended',$items);
	 			startRow+=10;
				endRow+=10;
				alert(startRow);
				var data={"startRow":startRow,"endRow":endRow};
				$.ajax({      
			        type:"post",  
			        url:"${pageContext.request.contextPath}/add/contents",      
			        data:data,      
			        success:function(addContents){   
			        	console.log(addContents);
			        },   
			        error:function(e){  
			            alert(e.responseText);  
			        }  
			    }); 
			}
		}); */
// 		$( document ).ready(function() {
// 		  	$('#upload-pop').hide();
// 		});
		
		function userPop(contentId,friendId){
			var url ="${pageContext.request.contextPath}/member/friend/"+friendId;
			$.ajax({      
		        type:"get",  
		        url:url,      
		        success:function(data,status){   
		        	if(status=='success'){
		        		if(data== 'friend')
		        			$('#friend_'+contentId).toggleClass('active');
		        		else if (data =='notYetFriend')
		        			$('#notYetFriend_'+contentId).toggleClass('active');
		        		else if (data=='disLike_'+contentId)
		        			$('#disLike').toggleClass('active');
		        	}
		        },   
		        error:function(xhr, textStatus, error){
		        	if(xhr.status=="500"){
		        		$(location).attr('href','${pageContext.request.contextPath}/auth/login/');
		        	}  
		        },
		        beforeSend : function(xmlHttpRequest){
	                   xmlHttpRequest.setRequestHeader("AJAX", "true"); // ajax 호출을  header에 기록
				}  
		    });
		}
		
		function addFriend(friendId){
// 			var data = $("#loginForm").serialize();
			var data ={ friendId: friendId};
			
			
			$.ajax({      
		        type:"post",  
		        url:"${pageContext.request.contextPath}/member/friend",      
		        data:data,      
		        success:function(args){   
		        	if(args.result)
						alert("친구신청이 완료되었습니다.");
					else 
						alert("친구신청 중 오류가 발생하였습니다.");
		        },   
		        error:function(xhr, textStatus, error){
		        	if(xhr.status=="500"){
		        		$(location).attr('href','${pageContext.request.contextPath}/auth/login/');
		        	}  
		        },
		        beforeSend : function(xmlHttpRequest){
	                   xmlHttpRequest.setRequestHeader("AJAX", "true"); // ajax 호출을  header에 기록
				}
		    });
			
			
			
			
/* 			$.post("member/friend", data ,function(data, status) {
// 				alert("Data: " + data + "\nStatus: " + status);
				if(status=='success'){
// 					console.log(data);
// 					if(data.result)
// 						$(location).attr('href','<c:url value="/"/>');
// 					else ;
						// TODO 로그인 false 구현
				} else ;
					// TODO 로그인 실패 구현
			}); */
		}
		
		function sendMessage(memberId){
			var url ='${pageContext.request.contextPath}/message/form?receiverId='+memberId;
			$(location).attr('href',url);
		} 
		

		function toggleLike(isAdd,contentId){
			$('#contentId').val(contentId);
			$('#isAdd').val(isAdd);
			var selectedItem = '#like_'+contentId;
			var selectedSpanItem = '#like_span_'+contentId;

			var data = $('#likeform').serialize();
			$.ajax({      
		        type:"post",  
		        url:"${pageContext.request.contextPath}/content/like",      
		        data:data,      
		        success:function(data,status){
		        	if(status =='success'){
		        		$(selectedItem).toggleClass("active");
			        	if(isAdd == 'true'){
			        		$(selectedItem).attr('href',"javascript:toggleLike('false',"+contentId+")");
			        		
			        		
			        	}else{
			        		$(selectedItem).attr('href',"javascript:toggleLike('true',"+contentId+")");
			        	}
			        	$(selectedSpanItem).text(data.likeCount);
		        	}
		        },   
		        error:function(xhr, textStatus, error){
		        	if(xhr.status=="500"){
		        		$(location).attr('href','${pageContext.request.contextPath}/auth/login/');
		        	}  
		        },
		        beforeSend : function(xmlHttpRequest){
	                   xmlHttpRequest.setRequestHeader("AJAX", "true"); // ajax 호출을  header에 기록
				}
		    });
/* 			$.ajax({      
		        type:"post",  
		        url:"${pageContext.request.contextPath}/content/like",      
		        data:data,      
		        success:function(data,status){
		        	if(status =='success'){
		        		$(selectdItem).toggleClass("active");
			        	if(isAdd == 'true'){
			        		$(selectdItem).attr('href',"javascript:toggleLike('false',"+contentId+")");
			        		
			        		data.likeCount
			        	}else{
			        		$(selectdItem).attr('href',"javascript:toggleLike('true',"+contentId+")");
			        	}
		        	}
		        },   
		        error:function(xhr, textStatus, error){
		        	if(xhr.status=="500"){
		        		$(location).attr('href','${pageContext.request.contextPath}/auth/login/');
		        	}  
		        },
		        beforeSend : function(xmlHttpRequest){
	                   xmlHttpRequest.setRequestHeader("AJAX", "true"); // ajax 호출을  header에 기록
				}
		    }); */
		}
		
		function clickpop(){
			
		}
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
</body>
</html>