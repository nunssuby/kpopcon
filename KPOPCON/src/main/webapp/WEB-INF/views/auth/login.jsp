<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sub.css">
</head>
<body>
<div id="sign">
	<header class="sign-header">
		<h1><img src="${pageContext.request.contextPath}/resources/img/sign_logo.png" alt="KPOPCON"></h1>
		<a class="sign-btn" href="${pageContext.request.contextPath}/auth/join">Join</a>
	</header>
	<section class="sign-content">
		<ul class="sign">
			<li>
			<article class="sign-in">
				<div class="sign-visual"></div>
				<form id="loginForm" name="loginForm"  action="javascript:isLogin();">
					<div class="sign-input-wrap">
						<span class="input">
							<label for="email">Email(ID)</label>
							<input id="email" type="text" name="email" required>
						</span>
						<span class="input">
							<label for="pw1">Password</label>
							<input id="pw1" type="text" name="password" required>
						</span>
						<input class="sign-submit" type="submit" value="Login">
						<span class="find-pw">
							<a href="#">Find Password?</a>
						</span>
					</div>
				</form>
				<div class="sns-wrap">
					<a href="javascript:googleLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns1.png" alt="goolge plus"></a>
					<a href="javascript:twitterLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns2.png" alt="twitter"></a>
					<a href="javascript:instagramLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns3.png" alt="instagram"></a>
					<a href="javascript:facebookLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns4.png" alt="facebook"></a>
				</div>
			</article>
			</li>
			<li>
				<article class="guide guide1">
					<div class="guide-visual">
						<img src="${pageContext.request.contextPath}/resources/img/guide_01_img.png" alt="">
					</div>
					<h2 class="title">
						<img src="${pageContext.request.contextPath}/resources/img/guide_01_title.png" alt="kpopcon이 뭐냐구요?">
					</h2>
					<div class="description">
						<img src="${pageContext.request.contextPath}/resources/img/guide_01_description.png" alt="KPOPCON은 K-culture에 깊은 관심을 가지고 있는 전 세계 모든 사람들이 함께 소통하며 서로의 문화와 관심사를 나누는 글로벌 SNS 입니다.">
					</div>
				</article>
			</li>
			<li>
				<article class="guide guide2">
					<h2 class="title">
						<img src="${pageContext.request.contextPath}/resources/img/guide_02_title.png" alt="">
					</h2>
					<div class="guide-visual">
						<img src="${pageContext.request.contextPath}/resources/img/guide_02_img.png" alt="">
					</div>
				</article>
			</li>
			<li>
				<article class="guide guide3">
					<h2>
						<img src="${pageContext.request.contextPath}/resources/img/guide_03_title.png" alt="">
					</h2>
					<div class="guide-visual">
						<img src="${pageContext.request.contextPath}/resources/img/guide_03_img.png" alt="">
					</div>
				</article>
			</li>
			<li>
				<article class="guide guide4">
					<h2>
						<img src="${pageContext.request.contextPath}/resources/img/guide_04_title.png" alt="">
					</h2>
					<div class="guide-visual">
						<img src="${pageContext.request.contextPath}/resources/img/guide_04_img.png" alt="">
					</div>
				</article>
			</li>
		</ul>
		<img class="guide-coachmark" src="${pageContext.request.contextPath}/resources/guide_04_coachmark.png" alt="">
		<footer class="sign-footer">
			<div class="banner-wrap">
				<a href="#"><img src="${pageContext.request.contextPath}/resources/sign_btn_appstore.png" alt="Availiable on App Store"></a>
				<a href="#"><img src="${pageContext.request.contextPath}/resources/sign_btn_googleplay.png" alt="Android app on Google Play"></a>
			</div>
		</footer>
	</section>
	<form:form id="snsLoginForm" name="snsLoginForm" method="POST" modelAttribute="member" >
		<input type="hidden" id="loginType" name="loginType" >
		<input type="hidden" id="snsId" name="snsId" >
	</form:form>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/oauth.js"></script>
	<script>
	$(document).ready(function(){
		$('.sign').bxSlider();
		
	});
	
	//여기서부터 개
		//onsubmit="validation()"
		$(function() {
			$("loginForm").validate();
		})
		// 		function validation(){
		// 			if ($("#password").val())

		// 		}

		function isLogin() {
			var data = $("#loginForm").serialize();
			$.ajax({      
		        type:"post",  
		        url:"${pageContext.request.contextPath}/auth/loginResult",      
		        data:data,      
		        success:function(args){   
		        	if(args.result)
						$(location).attr('href','<c:url value="/"/>');
					else 
						alert("id 혹은 패스워드를 확인해주세요");
		        },   
		        error:function(e){  
		            alert(e.responseText);  
		        }  
		    }); 
			
			
			
/* 		$.post("${pageContext.request.contextPath}/auth/loginResult", data ,function(data, status) {
// 				alert("Data: " + data + "\nStatus: " + status);
				if(status=='success'){
					console.log(data);
// 					console.log(data.result);
// 					data = $.parseJSON(data);
// 					alert(data.result);
// 					$.each(data, function(key, value){
// 					    alert('key:' + key + ' / ' + 'value:' + value);
// 					});
					if(data.result)
						$(location).attr('href','<c:url value="/"/>');
					else ;
						// TODO 로그인 false 구현
				} else ;
					// TODO 로그인 실패 구현
			}); */ 
		}

	</script>
	<script>
	$('#snsLoginForm').submit( function( e ) {
	  	  $.ajax({
	    	    url: '<c:url value="/auth/snsLoginResult"/>',
	    	    type: 'POST',
	    	    	data: new FormData( this ),
	    	    contentType: false,
	    	    processData: false,
	//     	    dataType: 'json',
	    	    success: callback
	    	});
	      e.preventDefault();
	  } );
		
	function callback(data, status){
  	if(status=='success'){
  		if(data.result)
			$(location).attr('href','<c:url value="/"/>');
		else 
			alert("해당 sns로 가입된 정보가 없습니다.");
  	}
  }
	
	OAuth.initialize('6H4QhBovJajmvuxej8-SeoUBq60');
	
	function instagramLogin() {
		OAuth.popup('instagram')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.email);
		        $('#loginType').val('IG');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#snsLoginForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
	
	function facebookLogin() {
		
	
		OAuth.popup('facebook')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.email);
		        $('#loginType').val('FB');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#snsLoginForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
	
	function googleLogin() {
		OAuth.popup('google_plus')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.email);
		        $('#loginType').val('GP');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#snsLoginForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
	
	
	function twitterLogin() {
		OAuth.popup('twitter')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.id);
		        console.log(response.email);
		        $('#loginType').val('TW');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#snsLoginForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
	</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>


