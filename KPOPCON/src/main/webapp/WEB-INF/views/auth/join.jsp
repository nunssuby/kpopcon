<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>		
	
	<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">
<title>KPOPCON</title>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sub.css">
</head>
<body>
<div id="sign">
	<header class="sign-header">
		<h1><img src="${pageContext.request.contextPath}/resources/img/sign_logo.png" alt="KPOPCON"></h1>
		<a class="sign-btn" href="${pageContext.request.contextPath}/auth/login">Login</a>
	</header>
	<section class="sign-content">
		<div class="sign">
			<article class="sign-up">
				<div class="sign-visual"></div>
				<form:form id="joinForm" name="joinForm" method="POST" modelAttribute="member" >
					<input type="hidden" id="loginType" name="loginType" value="KP">
					<input type="hidden" id="snsId" name="snsId" >
					<div class="sign-input-wrap">
						<span class="input">
							<label for="email">Email(ID)</label>
							<input id="email" type="text" name="email" required>
						</span>
						<span class="input">
							<label for="pw1">Password</label>
							<input id="pw1" type="text" name="password" required>
						</span>
						<span class="input">
							<label for="pw2">Verify Password</label>
							<input id="pw2" type="text" name="verify_password" required>
						</span>
						<input class="sign-submit" type="submit" value="Join">
					</div>
				</form:form>
				<div class="sns-wrap">
					<a href="javascript:googleLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns1.png" alt="goolge plus"></a>
					<a href="javascript:twitterLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns2.png" alt="twitter"></a>
					<a href="javascript:instagramLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns3.png" alt="instagram"></a>
					<a href="javascript:facebookLogin()"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_sns4.png" alt="facebook"></a>
				</div>
			</article>
		</div>
	</section>
	<footer class="sign-footer">
		<span class="description">Look around kpopcon</span>
		<div class="indicator">

		</div>
		<div class="banner-wrap">
			<a href="#"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_appstore.png" alt="Availiable on App Store"></a>
			<a href="#"><img src="${pageContext.request.contextPath}/resources/img/sign_btn_googleplay.png" alt="Android app on Google Play"></a>
		</div>
	</footer>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/default.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/oauth.js"></script>
	<script>
			$('#joinForm').submit( function( e ) {
		  	  $.ajax({
		    	    url: '<c:url value="/auth/member"/>',
		    	    type: 'POST',
		    	    	data: new FormData( this ),
		    	    contentType: false,
		    	    processData: false,
		//     	    dataType: 'json',
		    	    success: callback
		    	});
		      e.preventDefault();
		  } );
			
		function callback(data, status){
	    	if(status=='success'){
	    		
	    		if(data.isMember){
	    			alert("이미가입된 sns나 email 정보가 있습니다.");
	    		}
	    		if($.isNumeric(data.memberId)&&data.memberId>0){
	    			var url = '<c:url value="/member/"/>';
	    			url+= data.memberId;
	    			url+="/information/form";
	    			$(location).attr('href',url);
	    		}
	    	}
	    }
	</script>
	<script>
	OAuth.initialize('6H4QhBovJajmvuxej8-SeoUBq60');
	
	function instagramLogin() {
		OAuth.popup('instagram')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.email);
		        $('#loginType').val('IG');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#joinForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
	
	function facebookLogin() {
		
	
		OAuth.popup('facebook')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.email);
		        $('#loginType').val('FB');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#joinForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
	
	function googleLogin() {
		OAuth.popup('google_plus')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.email);
		        $('#loginType').val('GP');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#joinForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
	
	
	function twitterLogin() {
		OAuth.popup('twitter')
		.done(function(result) {
		    result.me()
		    .done(function (response) {
		        //this will display "John Doe" in the console
		        console.log(response);
		        console.log(response.id);
		        console.log(response.email);
		        $('#loginType').val('TW');
		        $('#snsId').val(response.id);
		        $('#email').val(response.email);
		        $('#joinForm').submit(); 
		        
		    })
		    .fail(function (err) {
		        //handle error with err
		    });
		})
		.fail(function (err) {
		  //handle error with err
		});
	}
/* 	function twitterLogin() {
		OAuth.redirect('twitter',
				"http://localhost:8080/kpopcon/auth/twitter");
	}

	OAuth.callback('twitter').done(function(result) {
		//use result.access_token in your API request
		//or use result.get|post|put|del|patch|me methods (see below)
		console.log("twitter result : "+result);
	}).fail(function(err) {
		//handle error with err
	});

	function facebookLogin() {
		OAuth.redirect('facebook',
				"http://localhost:8080/kpopcon/auth/sns");
	}

	OAuth.callback('facebook').done(function(result) {
		//use result.access_token in your API request
		//or use result.get|post|put|del|patch|me methods (see below)
		console.log("facebook result : "+result);
	}).fail(function(err) {
		//handle error with err
	});

	function googleLogin() {
		OAuth.redirect('google',
				"http://localhost:8080/kpopcon/auth/google");
	}

	OAuth.callback('google').done(function(result) {
		//use result.access_token in your API request
		//or use result.get|post|put|del|patch|me methods (see below)
		console.log("google result : "+result);
	}).fail(function(err) {
		//handle error with err
	});

	function instagramLogin() {
		OAuth.redirect('instagram',
				"http://localhost:8080/kpopcon/auth/instagram");
	}

	OAuth.callback('instagram').done(function(result) {
		//use result.access_token in your API request
		//or use result.get|post|put|del|patch|me methods (see below)
		console.log("instagram result : "+result);
	}).fail(function(err) {
		//handle error with err
	}); */
	</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</body>
</html>
