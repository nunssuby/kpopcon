package com.bestoneenc.kpopcon.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.Tag;

@Repository
public interface TagMapper {
	public int insertTag(Tag tag);
	public List<Tag> selectTagsByContent(int contentId);
	public int deleteTag(int tagId);
	public int deleteTags(int contentId);
}
