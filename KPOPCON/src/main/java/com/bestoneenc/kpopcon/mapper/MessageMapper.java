package com.bestoneenc.kpopcon.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.Message;

@Repository
public interface MessageMapper {

	public void insertMessage(Message message);
	public int updateMessage(Message message);
	public List<Message> selectSendMessages(int senderId);
	public List<Message> selectReceiveMessages(int receiverId);
	public int deleteMessage(int messageId);
}
