package com.bestoneenc.kpopcon.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.Friendship;
import com.bestoneenc.kpopcon.vo.Member;
import com.bestoneenc.kpopcon.vo.Message;

@Repository
public interface MemberMapper {
	public int selectMemberCount(String email);
	public Member selectMember(String email);
	public Member selectMemberById(int memberId);
	public Member selectMemberBySns(Member member);
	public void insertMember(Member member);
	public List<Member> selectMembers(String memberType);
	public int deleteMember(int memberId);
	public int updateMember(Member member);
	public int updateMemberBySetting(Member member);
	
	public void insertMessage(Message message);
	
	public void insertFriendship(Friendship friendShip);
	public int updateFriendship(Friendship friendShip);
	public List<Friendship> selectFriends(int memberId);
	public List<Friendship> selectFriendsByDisLike(Friendship friendShip);
	public List<Friendship> selectFriendsByStatus(Friendship friendShip);
	public Friendship selectFriend(Friendship friendShip);
}
