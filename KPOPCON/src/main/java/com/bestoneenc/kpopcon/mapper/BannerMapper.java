package com.bestoneenc.kpopcon.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.Banner;

@Repository
public interface BannerMapper {
	public void insertBanner(Banner banner);
	public Banner selectBanner(int bannerId);
	public int updateBanner(Banner banner);
	public int deleteBanner(int bannerId);
	public List<Banner> selectBanners(String bannerType);
}
