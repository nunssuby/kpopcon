package com.bestoneenc.kpopcon.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.AttachedFile;
import com.bestoneenc.kpopcon.vo.ContentRelationship;
import com.bestoneenc.kpopcon.vo.Scope;
import com.bestoneenc.kpopcon.vo.Tag;

@Repository
public interface ContentMapper {
	
	public void insertContent(Content content);
	public Content selectContent(int contentId);
	public int updateContent(Content content);
	public int deleteContent(int contentId);
	public List<Content> selectContents(Map queryMap);
	
//	public Content getContent();
	
	public List<Category> selectCategories();
	public List<ContentBox> selectContentBoxByMember(int MemberId);
	public List<Scope> selectScopes();
	
	
	
	public Scope selectScope(int scopeId);
	
	
	public int insertScope(Scope scope);
	
	
	public int insertFile(AttachedFile file);
	
	public int insertContentLikability(ContentRelationship contentRelationship);
	public int deleteContentLikability(ContentRelationship contentRelationship);
	public ContentRelationship selectContentLikability(ContentRelationship contentRelationship);
	public int insertContentShare(ContentRelationship contentRelationship);
	public ContentRelationship selectContentShare(ContentRelationship contentRelationship);
	public int updateLikeCount(Content content);
}
