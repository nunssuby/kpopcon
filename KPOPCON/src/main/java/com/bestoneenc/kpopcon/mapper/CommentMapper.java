package com.bestoneenc.kpopcon.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.Comment;

@Repository
public interface CommentMapper {
	public void insertComment(Comment comment);
	public int updateComment(Comment comment);
	public List<Comment> selectComments(int contentId);
	public Comment selectComment(int commentId);
}
