package com.bestoneenc.kpopcon.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class Content implements Serializable{
	
	int contentId;
	
	String title;
	String contentType;
	String ImageUrl;
	String youtubeLink;
	Category category;
	String description;
	ContentBox contentBox;
	String tagWithComma;
	List<Tag> tags;
	Scope scope;
	Member member;
	List<Comment> comments;
	
	String likability;
	int shareCount;
	int likeCount;
    int snsCount; 
    int commentCount; 
	String benefit;
	String status;
	String isBest;
	String isMagazine;
	String creationDate;
	
	String isShare;
	String isLike;
	String isSns;
	String isComment;
	
	int contentMemberId;
	String contentMemberName;
	
	List<AttachedFile> contentFiles;
	List<MultipartFile> files;
	AttachedFile mainFile;

	public Content(){
		member = new Member();
		contentFiles = new ArrayList();
		files = new ArrayList();
		tags = new ArrayList();
		shareCount = 0;
		likeCount =0;
	    snsCount =0; 
	    commentCount= 0; 
	}
	
	
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getImageUrl() {
		return ImageUrl;
	}
	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}
	public String getYoutubeLink() {
		return youtubeLink;
	}
	public void setYoutubeLink(String youtubeLink) {
		this.youtubeLink = youtubeLink;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public int getCategoryId(){
		return category.getValue();
	}
	
	public void setCategoryId(int categoryId){
		setCategory(Category.valueOf(categoryId));
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ContentBox getContentBox() {
		return contentBox;
	}
	public void setContentBox(ContentBox contentBox) {
		this.contentBox = contentBox;
	}
	public String getTagWithComma() {
		return tagWithComma;
	}
	public void setTagWithComma(String tagWithComma) {
		this.tagWithComma = tagWithComma;
	}
	public List<Tag> getTags() {
		return tags;
	}
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	public Scope getScope() {
		return scope;
	}
	public void setScope(Scope scope) {
		this.scope = scope;
	}
	public int getScopeId(){
		return scope.getValue();
	}
	public void setScopeId(int scopeId){
		setScope(Scope.valueOf(scopeId));
	}
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public int getMemberId(){
		return member.getMemberId();
	}
	public void setMemberId(int memberId){
		member.setMemberId(memberId);
	}
	
	public int getContentMemberId() {
		return contentMemberId;
	}


	public void setContentMemberId(int contentMemberId) {
		this.contentMemberId = contentMemberId;
	}


	public String getContentMemberName() {
		return contentMemberName;
	}


	public void setContentMemberName(String contentMemberName) {
		this.contentMemberName = contentMemberName;
	}


	public String getLikability() {
		return likability;
	}
	public void setLikability(String likability) {
		this.likability = likability;
	}
	public int getShareCount() {
		return shareCount;
	}
	public void setShareCount(int shareCount) {
		this.shareCount = shareCount;
	}
	public String getBenefit() {
		return benefit;
	}
	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsBest() {
		return isBest;
	}
	public void setIsBest(String isBest) {
		this.isBest = isBest;
	}
	public String getIsMagazine() {
		return isMagazine;
	}
	public void setIsMagazine(String isMagazine) {
		this.isMagazine = isMagazine;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String CreationDate) {
		this.creationDate = CreationDate;
	}
	
	public List<AttachedFile> getContentFiles() {
		return contentFiles;
	}
	public void setContentFiles(List<AttachedFile> contentFiles) {
		this.contentFiles = contentFiles;
	}
	public List<MultipartFile> getFiles() {
		return files;
	}
	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	public boolean isContent(){
		if(contentId>0)
			return true;
		return false;
	}
	
	public void tagWithCommaConvertToTagsInList(){
		if(tagWithComma != null){
			String[] tags = tagWithComma.split(",");
			
			for (String tag:tags){
				Tag contentTag = new Tag();
				contentTag.setTag(tag);
				this.tags.add(contentTag);
			}
		}
	}


	public List<Comment> getComments() {
		return comments;
	}


	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}


	public AttachedFile getMainFile() {
		return mainFile;
	}


	public void setMainFile(AttachedFile mainFile) {
		this.mainFile = mainFile;
	}


	


	public int getLikeCount() {
		return likeCount;
	}


	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}


	public int getSnsCount() {
		return snsCount;
	}


	public void setSnsCount(int snsCount) {
		this.snsCount = snsCount;
	}


	public int getCommentCount() {
		return commentCount;
	}


	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}


	public String getIsShare() {
		return isShare;
	}


	public void setIsShare(String isShare) {
		this.isShare = isShare;
	}


	public String getIsLike() {
		return isLike;
	}


	public void setIsLike(String isLike) {
		this.isLike = isLike;
	}


	public String getIsSns() {
		return isSns;
	}


	public void setIsSns(String isSns) {
		this.isSns = isSns;
	}


	public String getIsComment() {
		return isComment;
	}


	public void setIsComment(String isComment) {
		this.isComment = isComment;
	}
	
	
	
//	public List<Tag> stringArrayToListConvertForTag(String[] tagArray){
//		List<Tag> tags = new ArrayList<Tag>();
//		
//		for (String tag:tagArray){
//			Tag contentTag = new Tag();
//			contentTag.setTag(tag);
//			tags.add(contentTag);
//		}
//		
//		return tags;
//	}
	
}
