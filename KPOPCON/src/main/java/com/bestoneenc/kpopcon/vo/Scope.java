package com.bestoneenc.kpopcon.vo;

public enum Scope {
	PUBLIC(1), FRIEND(2), PRIVATE(3);
	
	private final int value;
	
	Scope(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	public static Scope valueOf(int value){
		switch(value){
		case 1 : return PUBLIC;
		case 2 : return FRIEND;
		case 3 : return PRIVATE;
		default : throw new AssertionError("Unknown value : " + value);
		}
	}
	
}
