package com.bestoneenc.kpopcon.vo;

import java.io.File;
import java.util.UUID;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class AttachedFile{

	private int fileId;
	private String originalName;
	private String uuidName;
	private String extension;
	private String originalFullName;
//	private String uuidFullName;
	private Content content;
	private int createdBy;
	
	public AttachedFile(){
		this.content = new Content();
	}
	public AttachedFile(MultipartFile multipartFile){
		
		originalName = StringUtils.getFilename(multipartFile.getOriginalFilename());
		uuidName = UUID.randomUUID().toString();
		uuidName = uuidName.replace("-", "");
		extension = StringUtils.getFilenameExtension(multipartFile.getOriginalFilename());
//		uuidFullName = uuidName+"."+extension;
		this.content = new Content();
	}
	
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}
	public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	public String getUuidName() {
		return uuidName;
	}
	public void setUuidName(String uuidName) {
		this.uuidName = uuidName;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extenSion) {
		this.extension = extenSion;
	}
	public Content getContent() {
		return content;
	}
	public void setContent(Content content) {
		this.content = content;
	}
	public int getContentId(){
		return this.content.getContentId();
	}
	public void setContentId(int contentId){
		this.content.setContentId(contentId);
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public String getOriginalFullName() {
		return originalFullName;
	}
	public void setOriginalFullName(String originalFullName) {
		this.originalFullName = originalFullName;
	}
	public String getUuidFullName() {
		return uuidName+"."+extension;
	}
//	public void setUuidFullName(String uuidFullName) {
//		this.uuidFullName = uuidFullName;
//	}
	
	
	
}
