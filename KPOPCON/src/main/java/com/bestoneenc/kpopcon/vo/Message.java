package com.bestoneenc.kpopcon.vo;

import java.io.Serializable;

public class Message implements Serializable{
	int messageId;
	int senderId;
	int receiverId;
	String message;
	String creationDate;
	String lastUpdateDate;
	String creationTime;
	Member sender;
	Member receiver;
	
	public Message(){
		sender = new Member();
		receiver = new Member();
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public Member getSender() {
		return sender;
	}
	public void setSender(Member sender) {
		this.sender = sender;
	}
	public Member getReceiver() {
		return receiver;
	}
	public void setReceiver(Member receiver) {
		this.receiver = receiver;
	}
}
