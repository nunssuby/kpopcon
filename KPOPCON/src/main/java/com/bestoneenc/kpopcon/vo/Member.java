package com.bestoneenc.kpopcon.vo;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class Member implements Serializable{
	int memberId;
	String email;
	String password;
	String name;
	String gender;
	String birthday;
//	String nationality_id;
	String image;
	String loginType;
	String benefit;
	String status;
	String checkMail;
	String introduction;
	String creationDate;
//	String theme_id;
	
	String memberType;
	String profileFileName;
	String snsId;
	
	MultipartFile file;
	
	// TODO : 차후에 객체화 해야지
	int nationalityId;
	int themeId;
	
	Nationality nationality;
	
	public Member(){
		this.memberId = 0;
		email="";
		password="";
	}
	
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	public String getBenefit() {
		return benefit;
	}
	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	
	
	public int getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(int nationality_id) {
		this.nationalityId = nationality_id;
	}
	
	

	public String getCheckMail() {
		return checkMail;
	}

	public void setCheckMail(String checkMail) {
		this.checkMail = checkMail;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public int getThemeId() {
		return themeId;
	}

	public void setThemeId(int themeId) {
		this.themeId = themeId;
	}
	
	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	
	

	public String getProfileFileName() {
		return profileFileName;
	}

	public void setProfileFileName(String profileFileName) {
		this.profileFileName = profileFileName;
	}
	

	public Nationality getNationality() {
		return nationality;
	}

	public void setNationality(Nationality nationality) {
		this.nationality = nationality;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getSnsId() {
		return snsId;
	}

	public void setSnsId(String snsId) {
		this.snsId = snsId;
	}

	public boolean isLogin(String email, String password){
		
//		System.out.println("this email : "+this.email);
//		System.out.println("email : "+email);
//		System.out.println("this password : "+this.password);
//		System.out.println("password : "+password);
		
		if(isEmail(email)&&isPassword(password))
			return true;
		
		return false;
	}
	
	public boolean isEmail(String email){
		if(this.email.equalsIgnoreCase(email))
			return true;
		return false;
	}
	
	public boolean isPassword(String password){
		if(this.password.equalsIgnoreCase(password))
			return true;
		return false;
	}
	
	public boolean isMember(){
		if(memberId>0)
			return true;
		return false;
	}
	
}
