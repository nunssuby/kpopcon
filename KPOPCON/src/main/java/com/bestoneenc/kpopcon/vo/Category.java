package com.bestoneenc.kpopcon.vo;

public enum Category {
	Video(1), Image(2), Text(3), Link(4), Audio(5);
	
	private final int value;
	
	Category(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	public static Category valueOf(int value){
		switch(value){
		case 1 : return Video;
		case 2 : return Image;
		case 3 : return Text;
		case 4 : return Link;
		case 5 : return Audio;
		default : throw new AssertionError("Unknown value : " + value);
		}
	}
}
