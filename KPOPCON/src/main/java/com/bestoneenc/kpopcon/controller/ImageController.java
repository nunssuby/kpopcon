package com.bestoneenc.kpopcon.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bestoneenc.kpopcon.service.AttachedFileService;
import com.bestoneenc.kpopcon.service.ContentService;
import com.bestoneenc.kpopcon.service.MemberService;
import com.bestoneenc.kpopcon.vo.AttachedFile;
import com.bestoneenc.kpopcon.vo.Member;

@Controller
@RequestMapping("/image/**")
public class ImageController {
private static final Logger logger = LoggerFactory.getLogger(ImageController.class);
	
	@Inject
	private FileSystemResource fsResource;
	@Autowired
	public AttachedFileService attachedFileService;
	@Autowired
	public MemberService memberService;
	
	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<InputStreamResource> download(HttpServletRequest request, 
			@PathVariable("id") int fileId) {
		
		logger.debug(" file :{}  ",  fileId);
		
		try {
			
			AttachedFile attachedFile = attachedFileService.getAttachedFile(fileId);
			
//			Member member = (Member) session.getAttribute("member");
//			int folderId = member.getMemberId();
			int folderId = attachedFile.getCreatedBy();
			String fileName = attachedFile.getUuidFullName();
			
			logger.debug(" folderId :{}  fileName : {}",  folderId, fileName);
			InputStreamResource resource = getFileContent(folderId,fileName);
			
			HttpHeaders responseHeaders = new HttpHeaders();
//			responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//			responseHeaders.set("Content-Type", "video/mpeg");
//			
			
			responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			responseHeaders.set("Content-Disposition", "attachment;filename="+fileName);
			responseHeaders.set("Content-Transfer-Encoding", "binary");
			
			
			logger.debug(" responseHeaders :{}  ",  responseHeaders);
			
		return new ResponseEntity<InputStreamResource>(resource, responseHeaders, HttpStatus.OK);
		} catch (IOException e) {
			HttpHeaders responseHeaders = new HttpHeaders();
			return new ResponseEntity<InputStreamResource>(null, responseHeaders, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/image/profile/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<InputStreamResource> downloadProfile(HttpServletRequest request,  HttpSession session,
			@PathVariable("id") int memberId) {
		
		logger.trace(" memberId : {}  ",  memberId);
		
		try {
			String folderName = memberId+"/profile/";
			Member member = memberService.getMember(memberId);
			String fileName = member.getProfileFileName();
			InputStreamResource resource = getFile(folderName,fileName);
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			responseHeaders.set("Content-Disposition", "attachment; filename="+fileName);
			responseHeaders.set("Content-Transfer-Encoding", "binary");
			
		return new ResponseEntity<InputStreamResource>(resource, responseHeaders, HttpStatus.OK);
		} catch (IOException e) {
			HttpHeaders responseHeaders = new HttpHeaders();
			return new ResponseEntity<InputStreamResource>(null, responseHeaders, HttpStatus.NOT_FOUND);
		}
	}

	private InputStreamResource getFileContent(int folderId,String fileName) throws IOException {
		String path = String.format("%s%d/%s", fsResource.getPath(), folderId,fileName);
		FileSystemResource resource = new FileSystemResource(path);
		return new InputStreamResource(resource.getInputStream());
	}
	//범용서을 위해 확장 
	private InputStreamResource getFile(String folderName, String fileName) throws IOException{
		String path = String.format("%s%s%s", fsResource.getPath(),folderName, fileName);
		FileSystemResource resource = new FileSystemResource(path);
		return new InputStreamResource(resource.getInputStream());
	}
}
