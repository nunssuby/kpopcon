package com.bestoneenc.kpopcon.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.bestoneenc.kpopcon.service.ContentService;
import com.bestoneenc.kpopcon.service.LoginService;
import com.bestoneenc.kpopcon.service.StatsService;
import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.Member;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@Autowired
	private ContentService contentService;
	@Autowired
	private StatsService statsService;

private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model, HttpSession session,
			HttpServletRequest request) {
	
		Map<String,String> queryMap = new HashMap<String,String>();
//		queryMap.put("startRow", "0");
//		queryMap.put("endRow", "10");
		ModelAndView mav = new ModelAndView("/home/main");
		Member member = (Member)session.getAttribute("member");
		if(member!=null)
			queryMap.put("memberId", Integer.toString(member.getMemberId()));
		
		String orderBy = request.getParameter("orderBy");
		
		logger.trace("orderBy : {}", orderBy);
		if(orderBy == null || "".equals(orderBy))
			orderBy ="creation_date";
		queryMap.put("orderBy", orderBy);
		
		List<Content> contents = contentService.getContents(queryMap);
//		List<Category> categories = contentService.getCategories();
		
		int totalLikeCount = statsService.getLikeCount();
		int totalCommentCount = statsService.getCommentCount();
		int totalShareCount = statsService.getShareCount();
		mav.addObject("contents",contents);
		mav.addObject("totalLikeCount",totalLikeCount);
		mav.addObject("totalCommentCount",totalCommentCount);
		mav.addObject("totalShareCount",totalShareCount);
		if(member!=null){
			mav.addObject("member",member);
			mav.addObject("isSession","true");
		}
//		mav.addObject("categories",categories);
		
		return mav;
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpSession session, HttpServletRequest request){
		String contextPath = request.getContextPath();
		logger.trace("contextPath {}", contextPath);
		ModelAndView mav = new ModelAndView(contextPath+"/");
		session.removeAttribute("member");
	    RedirectView rv = new RedirectView( contextPath );
	    rv.setExposeModelAttributes(true);
	    return (ModelAndView)new ModelAndView(rv);
	}
	
	@RequestMapping(value="/add/contents", method = RequestMethod.POST)
	public @ResponseBody List<Content> reloadContents(@RequestParam Map<String,String> queryMap){
		logger.trace("startRow  : {}  ",queryMap.get("startRow"));
		logger.trace("endRow  : {}  ",queryMap.get("endRow"));
		List<Content> addContents =  contentService.getContents(queryMap);
		return addContents;
	}
	
	@RequestMapping(value="/header", method = RequestMethod.GET)
	public ModelAndView header(){
		ModelAndView mav = new ModelAndView("/header");
		List<Category> categories = contentService.getCategories();
		mav.addObject("categories",categories);
		return mav;
	}

}
