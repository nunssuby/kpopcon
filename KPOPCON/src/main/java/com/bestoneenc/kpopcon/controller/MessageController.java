package com.bestoneenc.kpopcon.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bestoneenc.kpopcon.service.MemberService;
import com.bestoneenc.kpopcon.service.MessageService;
import com.bestoneenc.kpopcon.vo.Member;
import com.bestoneenc.kpopcon.vo.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@RequestMapping("/message/**")
public class MessageController {
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	@Autowired
	private MessageService messageService;
	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value="/message", method =RequestMethod.GET)
	public ModelAndView list(HttpSession session){
		ModelAndView mav = new ModelAndView("/message/list");
		Member member = (Member)session.getAttribute("member");
		List<Message> messages = messageService.getReceiveMessages(member.getMemberId());
		mav.addObject("messages",messages);
		return mav;
	}
	
	@RequestMapping(value="/message/form", method=RequestMethod.GET)
	public ModelAndView form(@RequestParam(value = "receiverId") int receiverId, HttpSession session){
		logger.trace("memberId {}", receiverId);
		ModelAndView mav = new ModelAndView("/message/create");
		mav.addObject("receiver", memberService.getMember(receiverId));
		
		Member member = (Member)session.getAttribute("member");
		List<Message> messages = messageService.getSendMessages(member.getMemberId());
		mav.addObject("messages", messages);
		return mav;
	}
	
	@RequestMapping(value="/message", method=RequestMethod.POST)
	public @ResponseBody Map<String,String> create(@ModelAttribute("message") Message message, HttpSession session){
		Member member = (Member)session.getAttribute("member");
		message.setSenderId(member.getMemberId());
		Map<String,String> resultMap = new HashMap<String,String>(); 
		message = messageService.createMessage(message);
		 if(message.getMessageId()>0)
			 resultMap.put("result", "true");
		 else
			 resultMap.put("result", "false");
		
		 return resultMap;
	}

}
