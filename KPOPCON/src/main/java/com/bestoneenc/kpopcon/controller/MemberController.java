package com.bestoneenc.kpopcon.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.bestoneenc.kpopcon.service.MemberService;
import com.bestoneenc.kpopcon.service.NationalityService;
import com.bestoneenc.kpopcon.vo.Banner;
import com.bestoneenc.kpopcon.vo.Friendship;
import com.bestoneenc.kpopcon.vo.Member;
import com.bestoneenc.kpopcon.vo.Nationality;

@Controller
@RequestMapping("/member/**")
public class MemberController {
	
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	
	@Autowired
	private MemberService memberService;
	@Autowired
	private NationalityService nationalityService;
	@Inject
	private FileSystemResource fsResource;

	@RequestMapping(value = "/member/form", method = RequestMethod.GET)
	public ModelAndView form(
			) {
		ModelAndView mav = new ModelAndView("/member/create");
		return mav;
	}
	
	@RequestMapping(value="/member/{id}",method=RequestMethod.GET)
	public ModelAndView show(
			@PathVariable("id") int memberId
			){
		ModelAndView mav = new ModelAndView("/member/show");
		Member member = memberService.getMember(memberId);
		mav.addObject("member",member);
		
		return mav;
	}
	
	@RequestMapping(value="/member/setting",method=RequestMethod.GET)
	public ModelAndView setting(
			HttpSession session
			){
		Member sesstionMember = (Member)session.getAttribute("member");
		
		ModelAndView mav = new ModelAndView("/member/setting/show");
		Member member = memberService.getMember(sesstionMember.getMemberId());
		Nationality nationality = nationalityService.getNationality(member.getNationalityId());
		member.setNationality(nationality);
		mav.addObject("member",member);
		
		return mav;
	}
	@RequestMapping(value="/member/setting" ,method=RequestMethod.POST)
	public ModelAndView updateSetting(@ModelAttribute Member member){
		logger.trace("memberName : {}", member.getName());
		logger.trace("memberCheckMail : {}", member.getCheckMail());
		logger.trace("introdution {}", member.getIntroduction());
		
		ModelAndView mav = new ModelAndView("redirect:/member/setting");
		
		
		if((member.getFile()!=null) && (member.getFile().getOriginalFilename() !="")){
			String directory = fsResource.getPath()+member.getMemberId()+File.separator+"profile"+File.separator;
			logger.trace("directory : {} ", directory);
			File dest= new File(directory);
			if (!dest.exists()) { //존재하지 않으면
				dest.mkdirs(); //생성
			}
			MultipartFile multipartFile = member.getFile();
			String extension = StringUtils.getFilenameExtension(multipartFile.getOriginalFilename());
			String fullName = member.getMemberId()+"."+extension;
			
			logger.trace("extension : {}, fullName : {} ", extension,fullName);
			try {
				multipartFile.transferTo(new File(directory + fullName));
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				logger.error("IllegalStateException : ", e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error("IOException : ", e);
			}
			member.setProfileFileName(fullName);
		}
		
		
		memberService.updateMemberBySetting(member);
		mav.addObject("member",member);
		return mav;
		
	}
	
	@RequestMapping(value="/member/point",method=RequestMethod.GET)
	public ModelAndView point(){
		ModelAndView mav = new ModelAndView("/member/point/show");
		return mav;
	}
	@RequestMapping(value="/member/password",method=RequestMethod.GET)
	public ModelAndView showPassword(){
		ModelAndView mav = new ModelAndView("/member/password/show");
		return mav;
	}
	

	
	@RequestMapping(value = "/member/{id}/information/form", method = RequestMethod.GET)
	public ModelAndView updateInformationForm(
			@PathVariable("id") int memberId, HttpSession session, HttpServletRequest request
			) {
		
		Member sessionMember = (Member) session.getAttribute("member");
		
		ModelAndView mav = new ModelAndView();
		if(sessionMember.getMemberId() == memberId){
			Member member = memberService.getMember(memberId);
			mav.setViewName("/member/update");
			logger.debug("m .");
		}
		else{
//			String previousUrl = request.getHeader("referer");
//			logger.debug("previousUrl {}.", previousUrl);
			mav.setViewName("/home/main");
		}
		
			 
//		mav.addObject("member", member);
		
		return mav;
	}
//	@RequestMapping(value = "/member/information",method={RequestMethod.POST,RequestMethod.PUT}, params="_method=PUT")
	@RequestMapping(value = "/member/information", method = RequestMethod.PUT)
	public ModelAndView  updateInformation(
			@ModelAttribute("member") Member member, BindingResult result
			) {
		
		if ( result.hasErrors() ){
			 
			logger.debug( "getName {}.",result.getAllErrors() );
			 
			}
		
		logger.debug("getName {}.", member.getName());
		logger.debug("getGender {}.", member.getGender());
		logger.debug("birthday {}.", member.getBirthday());
		logger.debug("getNationalityId {}.", member.getNationalityId());
		
		//Member member = memberService.getMember(memberId);
		Member updateMember = memberService.getMember(member.getMemberId());
		
		updateMember.setBirthday(member.getBirthday());
		updateMember.setNationalityId(member.getNationalityId());
		updateMember.setGender(member.getGender());
		updateMember.setName(member.getName());
		
		if(memberService.updateMember(updateMember)>0){
			if("KP".equals(member.getLoginType())){
//				TODO : 인증메일 발송
			} 
			
		}
		
		ModelAndView mav = new ModelAndView("auth/login");
		mav.addObject("member", member);
		
		return mav;
	}
	
	@RequestMapping(value="/member/email/{email}",method=RequestMethod.GET)
	public ModelAndView showByEmail(
			@PathVariable("email") String email
			){
		ModelAndView mav = new ModelAndView("/member/show");
		Member member = memberService.getMember(email);
		mav.addObject("member",member);
		
		return mav;
	}
	
	@RequestMapping(value="/member/result/{email}",method=RequestMethod.GET)
	public @ResponseBody  Map<String,String> checkEmail(
			@PathVariable("email") String email
			){
		
		logger.debug("email :  {}.", email);
		Map<String,String> resultMap = new HashMap();
		Member member = memberService.getMember(email);
		if(member ==null){
			logger.debug("erroraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa {}.", "hahahah");
		}
		
		return resultMap;
	}
	
	@RequestMapping(value="/member/friend", method = RequestMethod.POST)
	public @ResponseBody Map create(@ModelAttribute("friendShip") Friendship friendship , BindingResult result, HttpSession session) {
//		public @ResponseBody int create(@RequestParam("friendId") String friendId, HttpSession session) {	
		
//		logger.debug("friendId : {}", friendship.getFriendId());
		Member member = (Member) session.getAttribute("member");
		friendship.setMemberId(member.getMemberId());
		friendship.setDislike("false");
		friendship.setStatus("Requesting");
		
		Friendship reversFriendship = new Friendship();
		reversFriendship.setMemberId(friendship.getFriendId());
		reversFriendship.setDislike("false");
		reversFriendship.setStatus("Requested");
		reversFriendship.setFriendId(friendship.getMemberId());
		
		memberService.createFriendShip(friendship,reversFriendship);
		Map<String,String> resultMap = new HashMap<String,String>();
		if(friendship.getFriendshipId()>0)
			resultMap.put("result", "S");
		
		return resultMap;
	}
	
	@RequestMapping(value="/member/friend", method = RequestMethod.GET)
	public ModelAndView list(HttpSession session, @RequestParam("type") String friendType){
		logger.trace("friendType : {}",friendType);
		ModelAndView mav = new ModelAndView("/member/friend/list");
		Member member = (Member)session.getAttribute("member");
		List<Friendship> friends = new ArrayList<Friendship>();
		Friendship friendship = new Friendship();
		friendship.setMemberId(member.getMemberId());
		if("friend".equals(friendType)){
			friendship.setDislike("false");
			friends = memberService.getFriendsByDisLike(friendship);
		}else if("dislike".equals(friendType)){
			friendship.setDislike("true");
			friends = memberService.getFriendsByDisLike(friendship);
		}
		
//		List<Friendship> friends = memberService.getFriends(member.getMemberId());
		mav.addObject("friends",friends);
		return mav;
	}
	
	@RequestMapping(value="/member/friend/{id}", method=RequestMethod.GET)
	public @ResponseBody String showFriendship(@PathVariable("id") int friendId, HttpSession session){
		logger.trace("friendId : {}",friendId);
		Member member = (Member) session.getAttribute("member");
		Friendship friendship = new Friendship();
		friendship.setMemberId(member.getMemberId());
		friendship.setFriendId(friendId);
		String friendType = memberService.getFriendType(friendship);
		return friendType;
	}
	
}
