package com.bestoneenc.kpopcon.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bestoneenc.kpopcon.service.LoginService;
import com.bestoneenc.kpopcon.service.MemberService;
import com.bestoneenc.kpopcon.vo.Member;

@Controller
@RequestMapping("/auth/**")
public class AuthController {
	

private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
	@Autowired
	LoginService loginService;
	@Autowired
	MemberService memberService;
	
//	스토리보드 page2
	@RequestMapping(value = "/auth/join", method = RequestMethod.GET)
	public ModelAndView form(
			) {
		ModelAndView mav = new ModelAndView("/auth/join");
		return mav;
	}
	
	@RequestMapping(value = "/auth/login", method = RequestMethod.GET)
	public String showLoginForm() {
		
		logger.trace("LoginForm");

		return "auth/login";
	}

	@RequestMapping(value = "/auth/loginResult", method = RequestMethod.POST)
	public @ResponseBody Map<String,Boolean> isLogin(HttpSession session, 
									@RequestParam(value = "email") String email, 
									@RequestParam(value = "password") String password)
											throws IOException {
		logger.trace("in loginResult {}",email);
		
		Map<String, Boolean> result = new HashMap<String, Boolean>();

		try {
			boolean isLogin = loginService.isLogin(email, password);
			Member member = loginService.getMember(email);
			if (isLogin) {
				createSessionForMember(session,member);
			}

			result.put("result", isLogin);
		} catch (DataAccessException e) {
			result.put("result", false);
			result.put("dataException", true);
		}

		return result;

	}
	
	@RequestMapping(value = "/auth/snsLoginResult", method = RequestMethod.POST)
	public @ResponseBody Map<String,Boolean> isLoginBySns(HttpSession session, 
			@ModelAttribute("member") Member member)
											throws IOException {
		logger.trace("snsId : {} / loginType : {} ",member.getSnsId(), member.getLoginType());
		
		Map<String, Boolean> result = new HashMap<String, Boolean>();

		try {
			member = memberService.getMember(member);
			if (member !=null && member.getMemberId()>0) {
				createSessionForMember(session,member);
				result.put("result", true);
			}else{
				result.put("result", false);
			}
			
		} catch (DataAccessException e) {
			result.put("result", false);
			result.put("dataException", true);
		}

		return result;

	}
	
	//회원가입 
	@RequestMapping(value ="/auth/member", method =RequestMethod.POST)
	public @ResponseBody Map<String,String> create(
			@ModelAttribute("member") Member member, BindingResult result,HttpSession session){
		
		Map<String,String> resultMap = new HashMap();
		Member existMember = null;
		if(member.getEmail()!=null && !"".equals(member.getEmail())){
			logger.trace("mailcheck emil :  {}",member.getEmail());
			 existMember = memberService.getMember(member.getEmail());
		}
		else{
			logger.trace("snscheck emil :  {}",member.getEmail());
			logger.trace("snscheck snsId :  {}",member.getSnsId());
			logger.trace("snscheck loginType :  {}",member.getLoginType());
			existMember = memberService.getMember(member);
		}
			
		if(existMember ==null){
			memberService.createMember(member);
			logger.trace("memberId {}.", member.getMemberId());
			resultMap.put("memberId", Integer.toString(member.getMemberId()));
			createSessionForMember(session,member);
		}
		else{
			resultMap.put("isMember", "true");
		}
		
//		try{
//		}catch (Exception e){
//			logger.trace("error {}.", e.getStackTrace());
//		}
		return resultMap;
		
	}
	
	@RequestMapping(value="/auth/sns", method = RequestMethod.GET)
	public ModelAndView joinSns(){
		ModelAndView mav = new ModelAndView("/auth/sns");
		return mav;
	}
	
	private void createSessionForMember(HttpSession session, Member member){
//		HttpSession session = request.getSession();
		if(session.isNew() || session.getAttribute("member")== null){
			session.setAttribute("member", member);
		} else {
			// TODO 이렇게 안되게 구현하기
			System.out.println("isLogin ok session 생성안된기라");
		}
	}

}
