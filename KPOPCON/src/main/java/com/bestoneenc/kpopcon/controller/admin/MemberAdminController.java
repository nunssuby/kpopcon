package com.bestoneenc.kpopcon.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bestoneenc.kpopcon.mapper.MemberMapper;
import com.bestoneenc.kpopcon.service.MemberService;
import com.bestoneenc.kpopcon.vo.Banner;
import com.bestoneenc.kpopcon.vo.Member;


@Controller
@RequestMapping("/admin/member/**")
public class MemberAdminController {
	
	@Autowired
	MemberService memberService;
	
	@RequestMapping(value="/admin/member/{type}/list",method=RequestMethod.GET)
	public ModelAndView list(
			@PathVariable("type") String memberType){
		ModelAndView mav = new ModelAndView("/admin/member/list");
		
		List<Member> members = memberService.getMembers(memberType);
		mav.addObject("members",members);
		
		return mav;
		
	}
	
/*	@RequestMapping(value="/admin/member/{id}/status",method=RequestMethod.PUT)
	public ModelAndView update(
			@PathVariable("id") int memberId){
		
		System.out.println("memberId : "+memberId);
		
		memberService.updateMember(toggleMemberStatus(memberService.getMember(memberId)));
		
			
		ModelAndView mav = new ModelAndView("/admin/member/list");
		return mav;
	}*/
	
	@RequestMapping(value="/admin/member/{id}/status",method=RequestMethod.PUT)
	public @ResponseBody Map updateStatus(
			@PathVariable("id") int memberId){
		
		Map<String,String> resultMap = new HashMap<String,String>();
		int result = memberService.updateMember(toggleMemberStatus(memberService.getMember(memberId)));
		if (result==1){
			resultMap.put("result", "S");
		} else
			resultMap.put("result", "F");
		
		return resultMap;
	}
	
	private Member toggleMemberStatus(Member member){
		
		if("Y".equalsIgnoreCase(member.getStatus()))
			member.setStatus("N");
		else if("N".equalsIgnoreCase(member.getStatus()))
			member.setStatus("Y");
		
		return member;
	}
	
/*	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView post(
			@ModelAttribute("member") Member member, BindingResult result){
		
		System.out.println("email : "+member.getEmail());
		
		
		ModelAndView mav = new ModelAndView("/admin/member/list");
		return mav;
	}*/

}



