package com.bestoneenc.kpopcon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bestoneenc.kpopcon.mapper.MemberMapper;
import com.bestoneenc.kpopcon.mapper.MessageMapper;
import com.bestoneenc.kpopcon.vo.Member;
import com.bestoneenc.kpopcon.vo.Message;

@Service
public class MessageService {

	@Autowired
	private MessageMapper messageMapper;
	@Autowired
	private MemberMapper memberMapper;
	
	public Message createMessage(Message message){
		messageMapper.insertMessage(message);
		return message;
	}
	
	public List<Message> getReceiveMessages(int memberId){
		List<Message> messages = messageMapper.selectReceiveMessages(memberId);
		for(Message message:messages){
			Member sender = memberMapper.selectMemberById(message.getSenderId());
			Member receiver = memberMapper.selectMemberById(message.getReceiverId());
			message.setSender(sender);
			message.setReceiver(receiver);
		}
		return messages;
	}
	
	public List<Message> getSendMessages(int memberId){
		List<Message> messages = messageMapper.selectSendMessages(memberId);
		for(Message message:messages){
			Member sender = memberMapper.selectMemberById(message.getSenderId());
			Member receiver = memberMapper.selectMemberById(message.getReceiverId());
			message.setSender(sender);
			message.setReceiver(receiver);
		}
		return messages;
	}
}
