package com.bestoneenc.kpopcon.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.bestoneenc.kpopcon.controller.HomeController;
import com.bestoneenc.kpopcon.mapper.MemberMapper;
import com.bestoneenc.kpopcon.vo.Member;

@Service
public class LoginService {
	
	@Autowired
	private MemberMapper loginMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);
	
	public boolean isMember(String email) {
		int result = loginMapper.selectMemberCount(email);
		return result>0?true:false;
	}
	
	public boolean isLogin(String email, String password) {
		Member member;
		if(isMember(email)){
			member = loginMapper.selectMember(email);
			
			logger.info("member id {}.", member.getMemberId());
			return member.isPassword(password);
			
		} else
			return false;
			
	}
	
	public Member getMember(String email){
		Member member = new Member();
		if(isMember(email))
			member = loginMapper.selectMember(email);
		return member;
	}

}
