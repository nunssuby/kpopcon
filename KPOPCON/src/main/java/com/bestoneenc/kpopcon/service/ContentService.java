package com.bestoneenc.kpopcon.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bestoneenc.kpopcon.controller.ContentController;
import com.bestoneenc.kpopcon.mapper.ContentMapper;
import com.bestoneenc.kpopcon.mapper.AttachedFileMapper;
import com.bestoneenc.kpopcon.mapper.CommentMapper;
import com.bestoneenc.kpopcon.mapper.MemberMapper;
import com.bestoneenc.kpopcon.mapper.TagMapper;
import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.Comment;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.AttachedFile;
import com.bestoneenc.kpopcon.vo.ContentRelationship;
import com.bestoneenc.kpopcon.vo.Scope;
import com.bestoneenc.kpopcon.vo.Tag;
import com.bestoneenc.kpopcon.vo.Member;

@Service
public class ContentService {
	
	private static final Logger logger = LoggerFactory.getLogger(ContentService.class);

	@Autowired
	private ContentMapper contentMapper;
	@Autowired
	private TagMapper tagMapper;
	@Autowired
	private AttachedFileMapper fileMapper;
	@Autowired
	private MemberMapper memberMapper;
	@Autowired
	private CommentMapper commentMapper;
	
	
	@Transactional
	public Content createContent(Content content)  {
		logger.trace("memberType : {}" , content.getMember().getMemberType());
		if("SA".equalsIgnoreCase(content.getMember().getMemberType()))
			content.setIsMagazine("true");
		contentMapper.insertContent(content);
		if (content.isContent()) {
			logger.debug("tag count : {}", content.getTags().size());
			for (Tag tag : content.getTags()) {
				tag.setContentId(content.getContentId());
				tagMapper.insertTag(tag);
			}
			logger.debug("file count : {}", content.getContentFiles().size());
			for (AttachedFile file : content.getContentFiles() ){
				file.setContentId(content.getContentId());
				fileMapper.insertFile(file);
			}
		}

		return content;
	}
	public Content getContent(int contentId){
		Content content = contentMapper.selectContent(contentId);
		Member member = memberMapper.selectMemberById(content.getMemberId());
		content.setMember(member);
		List<Tag> tags = tagMapper.selectTagsByContent(contentId);
		content.setTags(tags);
		List<Comment> comments = commentMapper.selectComments(contentId);
		for(Comment comment:comments){
			Member commentedBy = memberMapper.selectMemberById(comment.getMemberId());
			comment.setMemberName(commentedBy.getName());
		}
		content.setComments(comments);
		List<AttachedFile> contentFiles =  fileMapper.selectFilesByContent(content.getContentId());
		content.setContentFiles(contentFiles);
		return content;
	}
	public int deleteContent(Content content){
		
		tagMapper.deleteTags(content.getContentId());
		fileMapper.deleteFiles(content.getContentId());
		return contentMapper.deleteContent(content.getContentId());
	}
	public List<Content> getContents(Map<String,String> queryMap){
		
		List<Content> contests = contentMapper.selectContents(queryMap);
		for(Content content:contests){
			Member member = memberMapper.selectMemberById(content.getMemberId());
			content.setContentMemberId(member.getMemberId());
			content.setContentMemberName(member.getName());
//			List<Tag> tags = tagMapper.selectTagsByContent(content.getContentId());
//			content.setTags(tags);
			List<AttachedFile> contentFiles = fileMapper.selectFilesByContent(content.getContentId());
			if(contentFiles.size()!=0)
				content.setMainFile(contentFiles.get(0));
			if(queryMap.get("memberId") != null){
				Member loginUser = memberMapper.selectMemberById(Integer.parseInt(queryMap.get("memberId")));
				ContentRelationship cR = new ContentRelationship();
				cR.setContentId(content.getContentId());
				cR.setMemberId(loginUser.getMemberId());
				ContentRelationship contentRelationship = contentMapper.selectContentLikability(cR);
				if(contentRelationship !=null)
					content.setIsLike("true");
				else
					content.setIsLike("false");
				
				List<Comment> comments = commentMapper.selectComments(content.getContentId());
				boolean isCommenter = false;
				
				String memberId = queryMap.get("memberId");
				logger.trace("contentId : {}",content.getContentId());
				logger.trace("memberId : {}",memberId);
				logger.trace("comments size : {}",comments.size());
				for(Comment comment:comments){
					if(memberId.equals(comment.getMemberId())){
						
					}
						isCommenter = true;
				}
				if(isCommenter)
					content.setIsComment("true");
				else
					content.setIsComment("false");
				
				logger.trace("setIsComment  : {}",content.getIsComment());
				
			}
				
		}
		
		return contests;
	}
	
	
	public List<Tag> getTagsByContent(int contentId){
		List<Tag> contentTags = tagMapper.selectTagsByContent(contentId);
		return contentTags;
	}
	
	public Scope getContentScope(int scopeId) {

		Scope contentScope = contentMapper.selectScope(scopeId);

		return contentScope;
	}
	public List<Scope> getContentScopes(){
		return contentMapper.selectScopes();
	}

	public ContentBox getContentBox(int contentBoxId) {

		ContentBox contentBox = new ContentBox();
		return contentBox;

	}
	
	public List<ContentBox> getContentBoxes(Member member){
		List<ContentBox> contentBoxes = contentMapper.selectContentBoxByMember(member.getMemberId());
		return contentBoxes;
	}
	
	public List<Category> getCategories(){
		return contentMapper.selectCategories();
	}
	
	public Comment createComment(Comment comment){
		commentMapper.insertComment(comment);
		Content content = contentMapper.selectContent(comment.getContentId());
		content.setCommentCount(content.getCommentCount()+1);
		contentMapper.updateContent(content);
		return comment;
	}
	
	public int createContentLikability(ContentRelationship contentRelationship, Content content){
		int increment = contentMapper.updateLikeCount(content);
		int addlike = contentMapper.insertContentLikability(contentRelationship);
		return  increment+addlike;
	}
	public int deleteContentLikability(ContentRelationship contentRelationship, Content content){
		int decrement = contentMapper.updateLikeCount(content);
		int cancel = contentMapper.deleteContentLikability(contentRelationship);
		return decrement+cancel;
	}
	public ContentRelationship selectContentLikability(ContentRelationship contentRelationship){
		return contentMapper.selectContentLikability(contentRelationship);
	}
	public int insertContentShare(ContentRelationship contentRelationship){
		return contentMapper.insertContentShare(contentRelationship);
	}
	public ContentRelationship selectContentShare(ContentRelationship contentRelationship){
		return contentMapper.selectContentShare(contentRelationship);
	}

}
