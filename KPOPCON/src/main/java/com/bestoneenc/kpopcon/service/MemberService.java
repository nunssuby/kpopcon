package com.bestoneenc.kpopcon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bestoneenc.kpopcon.mapper.MemberMapper;
import com.bestoneenc.kpopcon.vo.Friendship;
import com.bestoneenc.kpopcon.vo.Member;
import com.danidemi.jlubricant.slf4j.Logger;

@Service
public class MemberService {
	@Autowired
	MemberMapper memberMapper;
	
	public List<Member> getMembers(String memberType){
		return memberMapper.selectMembers(memberType);
	}
	
	public Member getMember(String email){
		return memberMapper.selectMember(email);
	}
	
	public Member getMember(int memberId){
		return memberMapper.selectMemberById(memberId);
	}
	
	public Member getMember(Member member){
		return memberMapper.selectMemberBySns(member);
	}
	
	public int updateMember(Member member){
		return memberMapper.updateMember(member);
	}
	
	public int updateMemberBySetting (Member member){
		return memberMapper.updateMemberBySetting(member);
	}
	
	public Member createMember(Member member){
		memberMapper.insertMember(member);
		return member;
	}
	
	public Friendship createFriendShip(Friendship friendship, Friendship reverFriendship){
		memberMapper.insertFriendship(friendship);
		memberMapper.insertFriendship(reverFriendship);
		return friendship;
	}
	public int updateFriendShip(Friendship friendship){
		return memberMapper.updateFriendship(friendship);
	}
	public List<Friendship> getFriends(int memberId){
		List<Friendship> friends = memberMapper.selectFriends(memberId);
		for(Friendship friendship:friends){
			Member friend = memberMapper.selectMemberById(friendship.getFriendId());
			friendship.setFriendName(friend.getName());
		}
		return friends;
	}
//	public Friendship getFriend(int memberId, int friendId){
//		return  memberMapper.selectFriend(memberId,friendId);
//	}
	
	public String getFriendType(Friendship friendship){
		Friendship friendships = memberMapper.selectFriend(friendship);
		String result ="notYetFriend";
		if(friendships == null){
			result = "notYetFriend";
		}else{
			String status = friendships.getStatus();
			if("Requesting".equals(status) || "Requested".equals(status) || "Friend".equals(status)){
				result = "friend";
			}else if("true".equals(friendships.getDislike()))
				result ="disLike";
		}
		
		return result;
	}
	
	public List<Friendship> getFriendsByDisLike(Friendship friendShip){
		List<Friendship> friends = memberMapper.selectFriendsByDisLike(friendShip);
		for(Friendship friendship:friends){
			Member friend = memberMapper.selectMemberById(friendship.getFriendId());
			friendship.setFriendName(friend.getName());
		}
		return friends;
	}
	public List<Friendship> getFriendsByStatus(Friendship friendShip){
		List<Friendship> friends = memberMapper.selectFriendsByStatus(friendShip);
		for(Friendship friendship:friends){
			Member friend = memberMapper.selectMemberById(friendship.getFriendId());
			friendship.setFriendName(friend.getName());
		}
		return friends;
	}
}
