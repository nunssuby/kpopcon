package com.bestoneenc.kpopcon.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	  @ContextConfiguration("classpath:com/bestoneenc/kpopcon/service/test-service-context.xml"),
@ContextConfiguration("classpath:com/bestoneenc/kpopcon/test-servlet-context.xml") 
})

public class HomeControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

//	@Test
//	public void test() {
//		fail("Not yet implemented"); // TODO
//	}
	@Test
	public void testValidatingEmail() throws Exception {
		String email = "nunssuby@kpopcon.com";
		String password = "1234";
		this.mockMvc
				.perform(
						post("/login/loginResult").param("email", email).param("password",password).accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.result").value(true));
	}

	@Test
	public void testInValidatingEmail() throws Exception {
		String email = "nunssuby@sindoh.com";
		String password = "1234";
		this.mockMvc
				.perform(
						post("/login/loginResult").param("email", email).param("password",password).accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.result").value(false));
	}
	
	@Test
	public void testValidatingPassword() throws Exception {
		String email = "nunssuby@kpopcon.com";
		String password = "1234";
		this.mockMvc
				.perform(
						post("/login/loginResult").param("email", email).param("password",password).accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.result").value(true));
	}
	
	@Test
	public void testInValidatingPassword() throws Exception {
		String email = "nunssuby@kpopcon.com";
		String password = "234";
		this.mockMvc
				.perform(
						post("/login/loginResult").param("email", email).param("password",password).accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.result").value(false));
	}

	@Test
	public void testIsLogin() throws Exception {
		String email = "nunssuby@kpopcon.com";
		String password = "1234";
		this.mockMvc
				.perform(
						post("/login/loginResult").param("email", email).param("password",password).accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.result").value(true));
	}
	
	
	
//	@Test(expected=DataAccessException.class)
//	public void testDataAccessException() throws Exception {
//		this.mockMvc
//		.perform(
//				post("/login/loginResult").param("email", "a").param("password","b").accept(
//						MediaType.APPLICATION_JSON))
//		.andExpect(status().isOk())
////		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
//		.andExpect(jsonPath("$.result").value("true"));
//	}

}
