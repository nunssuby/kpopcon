package com.bestoneenc.kpopcon.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;




import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.bestoneenc.kpopcon.vo.Member;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
	  @ContextConfiguration("classpath:com/bestoneenc/kpopcon/service/test-service-context.xml"),
@ContextConfiguration("classpath:com/bestoneenc/kpopcon/test-servlet-context.xml") 
})

public class ImageControllerTest {

	@Autowired
	private WebApplicationContext wac;

	@Inject
	private FileSystemResource fsResource;
	
	private MockMvc mockMvc;
	private File testFile;
	
	@Autowired
	private MockHttpSession session;
	
	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

//	@After
//	public void TearDown() throws Exception {
//		if (testFile !=null) testFile.delete();
//	}
	
	@Test
	public final void testDownload_FileNotExist_404() throws Exception {
		String id="100";
		mockMvc.perform(get("/image/{id}",id)
				)
				.andExpect(status().isNotFound());
	}
	
	@Test
	public final void testDownload_FileExistEmptyFile_200() throws Exception {
		String id="1";
		Member member = new Member();
		member.setMemberId(2);
		member.setEmail("nunssuby@kpopcon.com");
		
		session.setAttribute("member", member);
		MockMultipartFile file = new MockMultipartFile("file", "07414305-03af-4efb-860d-cf2548903ddb.jpg", null, "data".getBytes());
		String directory = fsResource.getPath()+member.getMemberId()+File.separator;
		File destination = new File(directory);
		System.out.println("directory : "+directory.toString());
		if (!destination.exists()) { //존재하지 않으면
			destination.mkdirs(); //생성
		}
		file.transferTo(new File(directory+"07414305-03af-4efb-860d-cf2548903ddb.jpg"));
		System.out.println("full : "+directory+"07414305-03af-4efb-860d-cf2548903ddb.jpg");
		
		
		mockMvc.perform(get("/image/{id}",id).session(session)
					)
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
					;
//					.andExpect(content().bytes(emptyData));
	}
	
	/*	@Test
	public final void testDownload_FileExist_200() throws Exception {
		byte[] contentData = new byte[] { 0x20, 0x21, 0x23 };
		testFile = DirectoryHelper.createTempFile(fsResource.getFile(), "contentTestFile", contentData);

		this.mockMvc.perform(get("/file").param("filename", testFile.getName()))
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
					.andExpect(content().bytes(contentData));
	}*/


}
