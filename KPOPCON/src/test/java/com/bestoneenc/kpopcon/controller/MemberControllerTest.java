package com.bestoneenc.kpopcon.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
		@ContextConfiguration("classpath:com/bestoneenc/kpopcon/service/test-service-context.xml"),
	@ContextConfiguration("classpath:com/bestoneenc/kpopcon/test-servlet-context.xml") 
})
public class MemberControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;
	
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testCreate() throws Exception{
		
		String email="nunssauby@kpopcon.com";
		String password="1234";
		mockMvc.perform(post("/member")
				.param("email", email)
				.param("password", password)
				)
				.andExpect(status().isOk())
				;
	}
	
	@Test
	public void testShow() throws Exception {
		String id ="1";
		mockMvc.perform(get("/member/{id}",id)
				)
				.andExpect(status().isOk())
						;
	}
	@Test
	public void testShowByEmail() throws Exception {
		String email ="1";
		mockMvc.perform(get("/member/{email}",email)
				)
				.andExpect(status().isOk())
						;
	}
	@Test
	public void testcheckEmail() throws Exception {
		String email ="nunssuby@kpopcon.com";
		mockMvc.perform(get("/member/result/{email}",email)
				)
				.andExpect(status().isOk())
						;
	}
	
	@Test
	public void testUpdateInformation() throws Exception {
		String memberId="1";
		String birthday ="19811206";
		String gender="남성";
		String name="김한성";
		String nationalityId="1";
		
		mockMvc.perform(put("/member/information")
				.param("memberId", memberId)
				.param("name", name)
				.param("gender", gender)
				.param("nationalityId", nationalityId)
				.param("birthday", birthday)
				).andExpect(status().isOk());
	}
	
	
	

}
