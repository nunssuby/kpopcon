package com.bestoneenc.kpopcon.controller.admin;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.io.FileInputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.bestoneenc.kpopcon.vo.Banner;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
		@ContextConfiguration("classpath:com/bestoneenc/kpopcon/service/test-service-context.xml"),
	@ContextConfiguration("classpath:com/bestoneenc/kpopcon/test-servlet-context.xml") 
})
public class BannerControllerTest {
	
	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testUpdateForm() throws Exception{
		String id = "1";
		mockMvc.perform(get("/admin/banner/{id}/form",id)
//				.param("bannerId", bannerId)
				)
				.andExpect(status().isOk())
				;
	}
	
	@Test
	public void testForm() throws Exception{
		mockMvc.perform(get("/admin/banner/form")
				)
				.andExpect(status().isOk())
				;
	}
	/*@Test
	public void testCreate() throws Exception{
		String title = "controller 테스트";
		String sequence = "2";
		String startDate ="2015-04-16";
		String endDate ="2015-04-25";
		String language="한쿡어";
		String isValid ="Y";
		String link = "http://www.naver.com/";
		String bannerType ="main";
		FileInputStream fis = null;
		// TODO : 개발환경이므로 나중에 제거해야함
		String OS = System.getProperty("os.name").toLowerCase();
//		System.out.print("OS :"  +OS);
		if (OS.indexOf("win") >= 0) {
			fis = new FileInputStream("C:\\WebInstall.log");
        } else if (OS.indexOf("mac") >= 0) {
        	fis = new FileInputStream("/Users/nunssuby/Desktop/KakaoTalk_Photo_2015-03-29-01-01-17.jpeg");
        } else {
            System.out.println("Your OS is not support!!");
        }
		MockMultipartFile multipartFile = new MockMultipartFile("file", fis);
	    mockMvc.perform(MockMvcRequestBuilders.fileUpload("/admin/banner")
	            .file(multipartFile)
	            .param("title", title)
	            .param("sequence", sequence)
	            .param("startDate", startDate)
	            .param("endDate", endDate)
	            .param("language", language)
	            .param("isValid", isValid)
	            .param("link", link)
	            .param("bannerType", bannerType)
	            .contentType(MediaType.MULTIPART_FORM_DATA))
				.andExpect(status().isOk())
				;
	}*/
/*	@Test
	public void testCreate() throws Exception{
		
		
		String title = "거창한 테스트 제목";
		String sequence = "3";
		String startDate ="2015-04-16";
		String endDate ="2015-04-25";
		String language="한쿡어";
		String isValid ="Y";
		String link = "http://www.naver.com/";
		String bannerType ="main";
		String file ="";
		mockMvc.perform(post("/admin/banner")
				.param("title", title)
	            .param("sequence", sequence)
	            .param("startDate", startDate)
	            .param("endDate", endDate)
	            .param("language", language)
	            .param("isValid", isValid)
	            .param("link", link)
	            .param("bannerType", bannerType)
	            .param("file", "C:\\WebInstall.log")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
	            
				)
			   .andExpect(status().isOk())
			   .andDo(MockMvcResultHandlers.print())
			   ;
	}*/
	@Test
	public void testCreate() throws Exception{
		String title = "거창한 테스트 제목";
		String sequence = "3";
		String startDate ="2015-04-16";
		String endDate ="2015-04-25";
		String language="한쿡어";
		String isValid ="Y";
		String link = "http://www.naver.com/";
		String bannerType ="main";
		MockMultipartFile file = new MockMultipartFile("file", "original_filename.ext", null, "data".getBytes());

		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/admin/banner")
	            .file(file)
	            .param("title", title)
	            .param("sequence", sequence)
	            .param("startDate", startDate)
	            .param("endDate", endDate)
	            .param("language", language)
	            .param("isValid", isValid)
	            .param("link", link)
	            .param("bannerType", bannerType)
	            .contentType(MediaType.MULTIPART_FORM_DATA)
				)
                .andExpect(redirectedUrl("/admin/banner/2"))
			   .andDo(MockMvcResultHandlers.print())
			   ;
	}
	
	@Test
	public void testShow() throws Exception{
		String bannerId="1";
		mockMvc.perform(get("/admin/banner/{id}",bannerId)
//				.param("bannerId", bannerId)
				)
				.andExpect(status().isOk())
				;
	}
	@Test
	public void testEditBanner() throws Exception{
//		String bannerId ="true";
//		mockMvc.perform(put("/admin/banner/banner")
//				.param("bannerId", bannerId)
//				)
//				.andExpect(status().isOk())
//				;
	}
	@Test
	public void testDelete() throws Exception{
		String bannerId="1";
		mockMvc.perform(delete("/admin/banner/{id}",bannerId)
//				.param("bannerId", bannerId)
				)
				.andExpect(status().isOk())
				;
	}
	
	@Test
	public void testList() throws Exception{
		
		String bannerType="main";
		
		mockMvc.perform(get("/admin/banner/{type}/list",bannerType)
//				mockMvc.perform(get("/admin/banner")
//				.param("bannerType", bannerType)
				)
				.andExpect(status().isOk())
				;
	}
	
	
	

}
