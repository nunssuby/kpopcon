package com.bestoneenc.kpopcon.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import java.io.FileInputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.Member;
import com.bestoneenc.kpopcon.vo.Scope;
import com.bestoneenc.kpopcon.vo.Tag;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
		@ContextConfiguration("classpath:com/bestoneenc/kpopcon/service/test-service-context.xml"),
	@ContextConfiguration("classpath:com/bestoneenc/kpopcon/test-servlet-context.xml") 
})
public class ContentControllerTest {

	@Autowired
	private WebApplicationContext wac;
	@Autowired 
	private MockHttpSession session;

	private MockMvc mockMvc;

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
		
	
	}
	
	@Test
	public void testForm() throws Exception {
		Member member = new Member();
		member.setMemberId(1);
		member.setEmail("nunssuby@kpopcon.com");
		session.setAttribute("member", member);
		String type="Video";
		mockMvc.perform(get("/content/form/{type}", type).session(session))
		  .andExpect(status().isOk())
		  .andExpect(model().attributeExists("categories"))
		  .andExpect(model().attributeExists("contentBoxes"))
		  .andExpect(model().attributeExists("contentScopes"))
		  ;
				
	}
	
	@Test
	public void testCreateNoImage() throws Exception{
		String title = "거창한 테스트 제목";
		String contentType = "Image";
		String categoryId ="1";
		String description ="설명";
		String contentBoxId ="3";
		String tagWithComma ="음식,맛집,진짜맛있는집";
		String scopeId = "2";
		String isSns = "false";
		
		Member member = new Member();
		member.setMemberId(2);
		member.setEmail("kpopcon@kpopcon.com");
		
		session.setAttribute("member", member);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/content").session(session)
	            .param("title", title)
	            .param("contentType", contentType)
	            .param("categoryId", categoryId)
	            .param("description", description)
	            .param("contentBoxId", contentBoxId)
	            .param("tagWithComma", tagWithComma)
	            .param("scopeId", scopeId)
	            .param("isSns", isSns)
	            .contentType(MediaType.MULTIPART_FORM_DATA)
				)
//                .andExpect(redirectedUrl("/content"))
			   .andDo(MockMvcResultHandlers.print())
			   ;
	}
	
	@Test
	public void testCreateImage() throws Exception{
		String title = "거창한 테스트 제목";
		String contentType = "Image";
		String categoryId ="1";
		String description ="설명";
		String contentBoxId ="3";
//		String tag ="음식,맛집,진짜맛있는집";
		String tagWithComma ="음식";
		String scopeId = "2";
		String isSns = "false";
		
		MockMultipartFile file = new MockMultipartFile("files[0]", "original_filename.ext", null, "data".getBytes());
		Member member = new Member();
		member.setMemberId(2);
		member.setEmail("kpopcon@kpopcon.com");
		
		session.setAttribute("member", member);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/content")
	            .file(file)
	            .session(session)
	            .param("title", title)
	            .param("contentType", contentType)
	            .param("categoryId", categoryId)
	            .param("description", description)
	            .param("contentBoxId", contentBoxId)
	            .param("tagWithComma", tagWithComma)
	            .param("scopeId", scopeId)
	            .param("isSns", isSns)
	            .contentType(MediaType.MULTIPART_FORM_DATA)
				)
//                .andExpect(redirectedUrl("/content"))
			   .andDo(MockMvcResultHandlers.print())
			   ;
	}
	
	@Test
	public void testCreateImages() throws Exception{
		String title = "거창한 테스트 제목";
		String contentType = "Image";
		String categoryId ="1";
		String description ="설명";
		String contentBoxId ="3";
		String tagWithComma ="음식,맛집,진짜맛있는집";
		String scopeId = "2";
		String isSns = "false";
		
		MockMultipartFile file1 = new MockMultipartFile("files[0]", "original_filename_file1.ext", null, "data".getBytes());
		MockMultipartFile file2 = new MockMultipartFile("files[1]", "original_filename_file2.ext", null, "data".getBytes());
		
		Member member = new Member();
		member.setMemberId(2);
		member.setEmail("nunssuby@kpopcon.com");
		
		session.setAttribute("member", member);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/content")
	            .file(file1)
	            .file(file2)
	            .session(session)
	            .param("title", title)
	            .param("contentType", contentType)
	            .param("categoryId", categoryId)
	            .param("description", description)
	            .param("contentBoxId", contentBoxId)
	            .param("tagWithComma", tagWithComma)
	            .param("scopeId", scopeId)
	            .param("isSns", isSns)
	            .contentType(MediaType.MULTIPART_FORM_DATA)
				)
//                .andExpect(redirectedUrl("/content"))
			   .andDo(MockMvcResultHandlers.print())
			   ;
	}
	
	@Test
	public void testList() throws Exception{
		String bannerType="main";
		
		mockMvc.perform(get("/content")
//				.param("bannerType", bannerType)
				)
				.andExpect(status().isOk())
				;
	}
	
	@Test
	public void testContenttLike() throws Exception{
		Member member = new Member();
		member.setMemberId(1);
		member.setEmail("nunssuby@kpopcon.com");
		session.setAttribute("member", member);
		
		String contentId="1";
		String isAdd = "false";

		
		
		mockMvc.perform(post("/content/like").session(session)
	            .param("contentId", contentId)
	            .param("isAdd", isAdd)
	            
				)
			   .andDo(MockMvcResultHandlers.print())
			   .andExpect(status().isOk())
			   ;
	}

/*	@Test
	public void testCreate() throws Exception {
		
		String title = "나만의 제목";
		String name = "파일테스트";
		String youtubeLink ="https://www.youtube.com/watch?v=P327irllaAI";
		String description ="설명";
		String tag ="음식,맛집,진짜맛있는집";
		String scope = "2";
		String category ="1";
		String contentBoxId ="3";
		String isSns = "false";
		
		FileInputStream fis = null;
		// TODO : 개발환경이므로 나중에 제거해야함
		String OS = System.getProperty("os.name").toLowerCase();
		System.out.print("OS :"  +OS);
		if (OS.indexOf("win") >= 0) {
			fis = new FileInputStream("C:\\WebInstall.log");
        } else if (OS.indexOf("mac") >= 0) {
        	fis = new FileInputStream("/Users/nunssuby/Desktop/KakaoTalk_Photo_2015-03-29-01-01-17.jpeg");
        } else {
            System.out.println("Your OS is not support!!");
        }
		MockMultipartFile multipartFile = new MockMultipartFile("file", fis);

	    mockMvc.perform(MockMvcRequestBuilders.fileUpload("/content")
	            .file(multipartFile)
	            .param("title", title)
	            .param("youtubeLink", youtubeLink)
	            .param("description", description)
	            .param("tag", tag)
	            .param("scope", scope)
	            .param("category", category)
	            .param("contentBoxId", contentBoxId)
	            .param("isSns", isSns)
	            .contentType(MediaType.MULTIPART_FORM_DATA))
				.andExpect(status().isOk())
//				.andExpect(jsonPath("$.result").value(false))
				;
	}*/
}
