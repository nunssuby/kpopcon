package com.bestoneenc.kpopcon.mapper;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.Message;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("../service/test-service-context.xml")
public class MessageMapperTest {
	
	@Autowired
	private MessageMapper messageMapper;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testInsertMessage() {
		Message message = new Message();
		message.setSenderId(1);
		message.setReceiverId(2);
		message.setMessage("메시지");
		messageMapper.insertMessage(message);
		assertThat(message.getMessageId(), is(2));
	}

	@Test
	public void testUpdateMessage() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectSendMessages() {
		List<Message> messages = messageMapper.selectSendMessages(1);
		assertThat(messages.size(), is(2));
	}

	@Test
	public void testSelectReceiveMessages() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteMessage() {
		fail("Not yet implemented");
	}

}
