package com.bestoneenc.kpopcon.mapper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.ContentRelationship;
import com.bestoneenc.kpopcon.vo.Scope;
import com.bestoneenc.kpopcon.vo.Tag;
import com.bestoneenc.kpopcon.vo.Member;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("../service/test-service-context.xml")
public class ContentMapperTest {
	
	@Autowired
	ContentMapper contentMapper;
	
	Content content;
	ContentBox contentBox;
	Member member;

	@Before
	public void setUp() throws Exception {
		String title = "나만의 제목";
		String name = "파일테스트";
		String youtubeLink = "https://www.youtube.com/watch?v=P327irllaAI";
		String description = "설명";
		String tag = "음식,맛집,진짜맛있는집";
		int scope = 2;
		int category = 1;
		String contentBoxId = "3";
		String isSNS = "false";
		String imageUrl = "url";

		contentBox = new ContentBox();
		member = new Member();
		member.setMemberId(1);

		content = new Content();
		content.setTitle(title);
		content.setYoutubeLink(youtubeLink);
		content.setCategory(Category.valueOf(category));
		content.setDescription(description);
		content.setScope(Scope.valueOf(scope));
		content.setContentBox(contentBox);
		content.setImageUrl(imageUrl);
		content.setTagWithComma("음식,맛집,진짜맛있는집");
//		String[] tags = ("음식,맛집,진짜맛있는집").split(",");
//		content.setTags(content.stringArrayToListConvertForTag(tags));
		content.tagWithCommaConvertToTagsInList();
		System.out.println("정리된 size"+content.getTags().size());
		content.setMember(member);
	}
	
	@Test
	public void testInsertContent(){
		contentMapper.insertContent(content);
		assertThat(content.getContentId(), is(5));
	}
	@Test
	public void testGetContent() {
		Content content = contentMapper.selectContent(1);
		assertThat(content.getTitle(), is("DB넣은 제목"));
	}
	@Test
	public void testUpdateContent(){
		Content content = contentMapper.selectContent(1);
		content.setTitle("제목 변경 테스트");
		int result = contentMapper.updateContent(content);
		assertThat(result, is(1));
		Content updatedContent = contentMapper.selectContent(1);
		assertThat(updatedContent.getTitle(), is("제목 변경 테스트"));
	}
	@Test
	public void testDeleteContent(){
		int result = contentMapper.deleteContent(2);
		assertThat(result, is(1));
		Content deletedContent = contentMapper.selectContent(2);
		assertThat(deletedContent , is(nullValue()));
	}

	
	@Test
	public void testInsertContentTag() {
		fail("Not yet implemented"); // TODO
	}
	
	@Test
	public void testSelectContents(){
		Map<String,String> queryMap = new HashMap<String,String>(); 
//		isMagazine ="true";
		queryMap.put("isMagazine", "true");
		List<Content> contents = contentMapper.selectContents(queryMap);
		assertThat(contents.size(),is(4));
	}
	
	
	@Test
	public void testSelectCategories() {

		List<Category> categories = contentMapper.selectCategories();
		for(Category category : categories){
			System.out.println("category : "+category);
		}
		
		assertThat(categories.get(0).getValue(), is(1));
		assertThat(categories.get(1).getValue(), is(2));
		assertThat(categories.get(2).getValue(), is(3));
		
		assertThat(categories.get(0), is(Category.Video));
		assertThat(categories.get(1), is(Category.Image));
		assertThat(categories.get(2), is(Category.Text));
		
	}
	
	@Test
	public void testGetContentScopes(){
		List<Scope> contentScopes = contentMapper.selectScopes();
		Scope firstScope = contentScopes.get(0);
		Scope secondScope = contentScopes.get(1);
		Scope thirdScope = contentScopes.get(2);
		
		assertThat(firstScope.getValue(), is(1));
		assertThat(firstScope, is(Scope.PUBLIC));
		assertThat(secondScope.getValue(), is(2));
		assertThat(secondScope, is(Scope.FRIEND));
		assertThat(thirdScope.getValue(), is(3));
		assertThat(thirdScope, is(Scope.PRIVATE));
	}

	@Test
	public void testGetContentBoxByMember() {
		List<ContentBox> contentBoxes =	contentMapper.selectContentBoxByMember(1);
		
		System.out.println("size : "+contentBoxes.size());
		
		ContentBox contentBox= contentBoxes.get(0);
		
		assertThat (contentBox.getBoxId(), is(1));
		assertThat (contentBox.getMember().getMemberId(), is(1));
		assertThat (contentBox.getName(), is("default"));
	}


	@Test
	public void testGetContentScope() {
		fail("Not yet implemented"); // TODO
	}

	

	@Test
	public void testInsertContentScope() {
		fail("Not yet implemented"); // TODO
	}
	
	
	@Test
	public void testInsertContentLikability(){
		ContentRelationship contentRelationship = new ContentRelationship();
		contentRelationship.setContentId(1);
		contentRelationship.setMemberId(1);
		 assertThat(contentMapper.insertContentLikability(contentRelationship),is(1));
	}
	
	@Test
	public void testSelectContentLikability(){
		ContentRelationship contentRelationship = new ContentRelationship();
		contentRelationship.setContentId(1);
		contentRelationship.setMemberId(1);
		ContentRelationship result = contentMapper.selectContentLikability(contentRelationship);
		assertThat(result.getCreationDate() , is("time"));
		
	}
	
	@Test
	public void testDeleteContentLikability(){
		ContentRelationship contentRelationship = new ContentRelationship();
		contentRelationship.setContentId(1);
		contentRelationship.setMemberId(1);
		contentMapper.deleteContentLikability(contentRelationship);
	}
	
	@Test
	public void testInsertContentShare(){
		ContentRelationship contentRelationship = new ContentRelationship();
		contentRelationship.setContentId(1);
		contentRelationship.setMemberId(1);
		contentMapper.insertContentShare(contentRelationship);
	}
	
	@Test
	public void testSelectContentShare(){
		ContentRelationship contentRelationship = new ContentRelationship();
		contentRelationship.setContentId(1);
		contentRelationship.setMemberId(1);
		contentMapper.selectContentShare(contentRelationship);
	}
	
	

}
