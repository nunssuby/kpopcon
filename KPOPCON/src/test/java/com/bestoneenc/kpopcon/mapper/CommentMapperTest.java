package com.bestoneenc.kpopcon.mapper;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("../service/test-service-context.xml")
public class CommentMapperTest {
	
	@Autowired
	private CommentMapper commentMapper;
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testInsertComment() {
		Comment comment = new Comment();
		comment.setContentId(1);
		comment.setMemberId(1);
		comment.setComment("댓글 들어간다. ");
		commentMapper.insertComment(comment);
		assertThat(comment.getCommentId(), is(2));
	}
	
	@Test
	public void testSelectComments() {
		List<Comment> comments = commentMapper.selectComments(1);
		assertThat(comments.size(), is(2));
	}

	@Test
	public void testSelectComment() {
		Comment comment = commentMapper.selectComment(1);
		assertThat(comment.getComment(),is("코멘트"));
	}

	@Test
	public void testUpdateComment() {
		Comment comment = new Comment();
		comment.setComment("내용수정");
		comment.setCommentId(1);
		assertThat(commentMapper.updateComment(comment), is(1));
	}

	

}








