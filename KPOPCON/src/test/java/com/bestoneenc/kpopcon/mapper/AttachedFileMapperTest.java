package com.bestoneenc.kpopcon.mapper;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.AttachedFile;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("../service/test-service-context.xml")
public class AttachedFileMapperTest {

	@Autowired
	AttachedFileMapper fileMapper;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testInsertFile() {
		AttachedFile file = new AttachedFile();
		file.setExtension("jpg");
		file.setUuidName(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		file.setOriginalName("진짜파일명");
		file.setContentId(3);
		fileMapper.insertFile(file);
		assertThat(file.getFileId(),is(4));
	}

	@Test
	public void testSelectFile() {
		AttachedFile file = fileMapper.selectFile(1);
		assertThat(file.getOriginalName(),is("파일명1"));
		
	}

	@Test
	public void testSelectFilesByConttent() {
		List<AttachedFile> list = fileMapper.selectFilesByContent(1);
		assertThat(list.size(), is(3));
		assertThat(list.get(0).getFileId(),is(1));
		assertThat(list.get(0).getUuidName(),is("07414305-03af-4efb-860d-cf2548903ddb"));
	}

	@Test
	public void testDeleteFile() {
		AttachedFile file = new AttachedFile();
		file.setExtension("jpg");
		file.setUuidName(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		file.setOriginalName("진짜파일명");
		file.setContentId(3);
		fileMapper.insertFile(file);
		assertThat(fileMapper.deleteFile(file.getFileId()),is(1));
	
	}

	@Test
	public void testDeleteFiles() {
		assertThat(fileMapper.deleteFiles(1), is(3));
	}

}
