package com.bestoneenc.kpopcon.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.AttachedFile;
import com.bestoneenc.kpopcon.vo.Scope;
import com.bestoneenc.kpopcon.vo.Tag;
import com.bestoneenc.kpopcon.vo.Member;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("test-service-context.xml")
public class ContentServiceTest {

	@Autowired
	ContentService contentService;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testCreateContent() throws Exception {

		String title = "나만의 제목";
		String name = "파일테스트";
		String youtubeLink = "https://www.youtube.com/watch?v=P327irllaAI";
		String description = "설명";
		String tag = "음식,맛집,진짜맛있는집";
		int scope = 2;
		int category = 1;
		String contentBoxId = "3";
		String isSNS = "false";
		String imageUrl = "url";

		ContentBox contentBox = new ContentBox();
		Member member = new Member();
		member.setMemberId(1);

		Content content = new Content();
		content.setTitle(title);
		content.setYoutubeLink(youtubeLink);
		content.setCategory(Category.valueOf(category));
		content.setDescription(description);
		content.setScope(Scope.valueOf(scope));
		content.setContentBox(contentBox);
		content.setImageUrl(imageUrl);
		content.setTagWithComma("음식,맛집,진짜맛있는집");
//		String[] tags = ("음식,맛집,진짜맛있는집").split(",");
//		content.setTags(content.stringArrayToListConvertForTag(tags));
		content.tagWithCommaConvertToTagsInList();
		content.setMember(member);
		
		AttachedFile file = new AttachedFile();
		file.setExtension("jpg");
		file.setUuidName(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		file.setOriginalName("진짜파일명");
		List<AttachedFile> contentFiles = new ArrayList();
		contentFiles.add(file);
		content.setContentFiles(contentFiles);

		contentService.createContent(content);
		assertThat(content.getContentId(), is(5));
		
		assertThat(content.getTags().get(0).getTag(), is(contentService.getTagsByContent(content.getContentId()).get(0).getTag()));
		assertThat(content.getTags().get(1).getTag(), is(contentService.getTagsByContent(content.getContentId()).get(1).getTag()));
		assertThat(content.getTags().get(2).getTag(), is(contentService.getTagsByContent(content.getContentId()).get(2).getTag()));
		assertThat(contentService.getContent(5).getTitle(),is("나만의 제목"));

	}
	
	@Test
	public void testGetContent() throws Exception {
		Content content = contentService.getContent(1);
		assertThat(content.getTitle(), is("DB넣은 제목"));
				
	}
	@Test
	public void testDeleteContent() {
		Content content = contentService.getContent(2);
		int result = contentService.deleteContent(content);
		assertThat(result , is(1));
	}
	@Test
	public void testGetTagsByContent() throws Exception {
		List<Tag> contentTags = contentService.getTagsByContent(1);

		assertThat(contentTags.get(0).getTag(), is("DB태그1"));
		assertThat(contentTags.get(1).getTag(), is("DB태그2"));
		assertThat(contentTags.get(2).getTag(), is("DB태그3"));
	}
	@Test
	public void testDeleteContentAddedTags(){
		Content content = contentService.getContent(1);
		int result = contentService.deleteContent(content);
		assertThat(result , is(1));
	}
	
	
	@Test
	public void testGetCategories() throws Exception {
		List<Category> categories = contentService.getCategories();
		
		for(Category category : categories){
			System.out.println("category : "+category);
		}
		
		Category firstCategory = categories.get(0);
		Category secontCategory = categories.get(1);
		Category thirdCategory = categories.get(2);
		
		assertThat(firstCategory.getValue(), is(1));
		assertThat(secontCategory.getValue(), is(2));
		assertThat(thirdCategory.getValue(), is(3));
		
		assertThat(firstCategory, is(Category.Video));
		assertThat(secontCategory, is(Category.Image));
		assertThat(thirdCategory, is(Category.Text));
		
	}

	
	@Test
	public void testGetContents() throws Exception {
		Map<String,String> queryMap = new HashMap<String,String>();
		List<Content> contents = contentService.getContents(queryMap);
		assertThat(contents.size(), is(4));
		for(Content content : contents){
			if(content.getContentId() ==1){
				assertThat(content.getTags().size(), is(3));
				for(Tag tag : content.getTags()){
				}
			}
		}
	}
	

	
	
}
