

insert into contents
		(member_id,category_id,title,youtube_link,image_url,description,scope_id,likability,share_count,benefit,status,is_best,creation_date)
		values
		(1,1,'DB넣은 제목','DB넣은 youtube내용','DB넣은 이미지 url','DB 직접 넣은 설명',1,null,0,null,null,null,now());

insert into contents
(member_id,category_id,title,youtube_link,image_url,description,scope_id,likability,share_count,benefit,status,is_best,creation_date)
values
(1,1,'하위폴더 없는 놈 테스트 위해 필요','DB넣은 youtube내용','DB넣은 이미지 url','DB 직접 넣은 설명',1,null,0,null,null,null,now());

insert into contents
(member_id,category_id,title,youtube_link,image_url,description,scope_id,likability,share_count,benefit,status,is_best,creation_date)
values
(1,1,'태그 delete를 위한 제목','태그위한 youtube내용','DB넣은 이미지 url','DB 직접 넣은 설명',1,null,0,null,null,null,now());
		
insert into contents
(member_id,category_id,title,youtube_link,image_url,description,scope_id,likability,share_count,benefit,status,is_best,creation_date)
values
(1,1,'태그 insert를 위한 제목','태그위한 youtube내용','DB넣은 이미지 url','DB 직접 넣은 설명',1,null,0,null,null,null,now());
		
insert into content_tags(content_id,tag,creation_date)
		values(1,'DB태그1',now());
insert into content_tags(content_id,tag,creation_date)
		values(1,'DB태그2',now());
insert into content_tags(content_id,tag,creation_date)
		values(1,'DB태그3',now());
		
insert into contents_box(member_id, name, creation_date)
		values(1,'default',now());
		
				
insert into content_files(content_id,original_name,uuid_name,extension,creation_date,last_update_date)
	values(1,'파일명1','07414305-03af-4efb-860d-cf2548903ddb','jpg',now(),now());
insert into content_files(content_id,original_name,uuid_name,extension,creation_date,last_update_date)
	values(1,'파일명2','08414305-03af-4efb-860d-cf2548903ddb','jpg',now(),now());
insert into content_files(content_id,original_name,uuid_name,extension,creation_date,last_update_date)
	values(1,'파일명3','09414305-03af-4efb-860d-cf2548903ddb','jpg',now(),now());
	
insert into comments(content_id, member_id, comment, is_secret, creation_date, last_update_date)
	values(1,1,'코멘트','false',now(),now() );
